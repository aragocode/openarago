
let upgrade_1_to_2 dbh version =
  EzPG.upgrade ~dbh ~version
    ~downgrade:[
      "drop table token_updates";
      "drop table contract_updates";
      "drop table wallets_updates";
      "drop table accounts";
      "drop table tokens";
      "drop table operators";
      "drop table meta_tokens";
      "drop table state"
    ] [

    (* Global (partial) state of the smart contract *)
    {|create table state(
      version bigint not null default 0,
      company_wallet varchar not null default '',
      admin_wallet varchar not null default '',
      number_of_tokens bigint not null default 0,
      max_included_token_id bigint not null default -1,
      max_included_meta_token_id bigint not null default -1,
      max_confirmed_token_id bigint not null default -1,
      max_confirmed_meta_token_id bigint not null default -1,
      contract_address varchar not null default '',
      paused boolean not null default true,
      confirmations smallint not null default 0,
      last timestamp not null default now())|};

    (* Global table of tokens:
       - insertions made on mint,
       - (statuses) updates made on mints and on transfers by main_handler *)
    {|create table tokens(
      token_id bigint primary key,
      block varchar not null,
      level int not null,
      tsp timestamp not null,
      confirmed boolean not null default false,
      last_block varchar not null,
      last_level int not null,
      last timestamp not null,
      transaction varchar not null,
      owner varchar not null,
      meta_token_id bigint not null,
      burnt boolean not null default false)|};


    (* Global table of operators:
       - insertions made on update_operators calls,
       - (statuses) updates made on update_operators calls by main_handler *)
    {|create table operators(
      token_id bigint not null,
      owner varchar not null,
      operators varchar[] not null default '{}',
      last_block varchar not null,
      last_level int not null,
      last timestamp not null,
      primary key (token_id, owner))|};

    (* Global table of tokens:
       - insertions made on mint,
       - (statuses) updates made on mints by main_handler *)
    {|create table meta_tokens(
      meta_token_id bigint primary key,
      current_supply bigint not null default 0,
      info jsonb not null,
      block varchar not null,
      level int not null,
      tsp timestamp not null,
      confirmed boolean not null default false,
      last_block varchar not null,
      last_level int not null,
      last timestamp not null,
      transaction varchar not null)|};

    (* Global table of seen accounts:
       - insertions made on mint, update_operators, transfer, managed,
       - (statuses) updates made on mints by main_handler *)
    {|create table accounts(
      address varchar primary key,
      last_block varchar not null,
      last_level int not null,
      last timestamp not null,
      managed boolean not null,
      tokens bigint[] not null default '{}')|};

    (* mint and burn *)
    {|create table contract_updates(

      transaction varchar not null,
      index int not null,
      block varchar not null,
      level int not null,
      main boolean not null default false,
      tsp timestamp not null,
      confirmed boolean not null default false,
      mint jsonb,
      burn jsonb,
      primary key (block, index))|};

    (* transfer and update *)
    {|create table token_updates(
      transaction varchar not null,
      index int not null,
      position int not null,
      block varchar not null,
      level int not null,
      main boolean not null default false,
      tsp timestamp not null,
      confirmed boolean not null default false,

      owner varchar not null,
      token_id bigint not null,
      destination varchar,
      operator varchar,
      add boolean,
      primary key (block, index, position))|};

    (* Different kinds of wallets or contract update
       (set managed, confirm admin/company wallet, pause/unpause) *)
    {|create type wallets_update_enum AS ENUM (
      'Confirm_admin',
      'Confirm_company_wallet',
      'Pause',
      'Unpause',
      'Manage'
    )|};

    (* set managed, confirm admin/company wallet, pause/unpause *)
    {|create table wallets_updates(
      transaction varchar not null,
      index int not null,
      block varchar not null,
      level int not null,
      main boolean not null default false,
      tsp timestamp not null,
      confirmed boolean not null default false,
      position int not null,

      address varchar not null,
      update_kind wallets_update_enum not null,
      primary key (block, index, position))|};
]

let upgrades =
  let last_version = fst List.(hd @@ rev !Versions.upgrades) in
  !Versions.upgrades @ [
    last_version + 1, upgrade_1_to_2;
  ]

let () =
  EzPGUpdater.main Cconfig.database ~upgrades
