open Mligo
include Fa2_operator_lib

(* range of nft tokens *)

(**
Retrieve the balances for the specified tokens and owners
@return callback operation
*)
let get_balance (p : balance_of_param) (ledger : ledger) : operation =
  let to_balance (r : balance_of_request) =
    match Big_map.find_opt r.token_id ledger with
    | None -> (failwith fa2_token_undefined : balance_of_response)
    | Some o ->
      let bal = if o = r.owner then 1n else 0n in
      { request = r; balance = bal } in
  let responses = List.map to_balance p.requests in
  Tezos.transaction responses 0u p.callback

(**
Update ledger balances according to the specified transfers. Fails if any of the
permissions or constraints are violated.
@param txs transfers to be applied to the ledger
@param validate_op function that validates if the tokens from the particular owner can be transferred.
 *)
let transfer (txs : transfer list) (validate_op : operator_validator)
    (s : storage) : ledger =
  (* process individual transfer *)
  let make_transfer (l, tx : ledger * transfer) =
    List.fold (fun (ll, dst : ledger * transfer_destination) ->
        match Big_map.find_opt dst.token_id ll with
        | None -> (failwith fa2_token_undefined : ledger)
        | Some o ->
          if dst.amount = 0n then ll
          else if dst.amount <> 1n then (failwith fa2_insufficient_balance : ledger)
          else if o <> tx.from_ then (failwith fa2_insufficient_balance : ledger)
          (* TODO: The best error above is likely fa2_not_owner,
             but the standard is under-specified ? *)
          else
            let () = validate_op o Tezos.sender dst.token_id s in
            Big_map.update dst.token_id (Some dst.to_) ll)
      tx.txs l in
  List.fold make_transfer txs s.ledger

let get_metadata (token_id : token_id) (storage : storage) : token_metadata =
  let meta_token_id = get_meta_token_id storage token_id in
  match Big_map.find_opt meta_token_id storage.meta_token_info with
    | None -> (failwith "NO_DATA" : token_metadata)
    | Some token_info  -> { token_info; token_id }

let update_managed (m : managed_storage) (pl : managed_param list) : managed_storage =
  List.fold (fun ((m, p) : managed_storage * managed_param) ->
      let chain_id = Tezos.chain_id in
      let addr : address = Tezos.address (Tezos.implicit_account (Crypto.hash_key p.pk)) in
      let msg = Bytes.pack (Tezos.self_address, chain_id) in
      if Crypto.check p.pk p.signed_msg msg then Big_map.add addr () m
      else (failwith "INVALID_SIGNED_MESSAGE" : managed_storage)
    ) pl m

let fail_if_not_company_wallet (a : storage) : unit =
  if Tezos.sender <> a.company_wallet then failwith "NOT_A_COMPANY_WALLET" else ()

let fa2_main (param, storage : fa2_entry_points * storage) : (operation  list) * storage =
  match param with
  | Transfer txs ->
    let ledger =
      transfer txs default_operator_validator storage in
    ([] : operation list), { storage with ledger }
  | Balance_of p ->
    let op = get_balance p storage.ledger in
    [ op ], storage
  | Update_operators ops ->
    let operators = fa2_update_operators storage.operators ops storage in
    let storage = { storage with operators } in
    ([] : operation list), storage
  | Managed add ->
    let () = fail_if_not_company_wallet storage in
    let managed = update_managed storage.managed add in
    let storage = { storage with managed } in
    ([] : operation list), storage

type nft_entry_points =
  | Fa2 of fa2_entry_points
  | Token_metadata of token_metadata_param

let nft_token_main (param, storage : nft_entry_points * storage)
  : (operation  list) * storage =
  match param with
  | Fa2 fa2 -> fa2_main (fa2, storage)
  | Token_metadata p ->
    let meta = get_metadata p.token_id storage in
    let () = p.handler meta in
    ([] : operation list), storage
