/****************************************************************************************************************
This file's goal is twofold:
- Show how to use different entrypoints of the NFT smart contract, via the JS/Typescript interface library
- Test different corner cases of the smart contract's entrypoints, and of the JS/Typescript library itself

Note that:
- Replaying all the tests takes some time
- If a test fails and it is not expected to, it may be because the operation is not injected/included for some
reasons (the granadanet node is down, no baker included the operation, ...)
****************************************************************************************************************/

const nft = require("../dist/fa2_nft.js");

const { TezosToolkit } = require('@taquito/taquito');
const { InMemorySigner, importKey } = require('@taquito/signer');

const assert = require('assert').strict; // used for assertions in the tests
const sleep = require('sleep');
const { RpcClient } = require('@taquito/rpc');
const crypto = require("crypto")

/**
 * Global config. Make sure to:
 * - modify the smart contract address if a new one is originated
 * - set another node address if the given one is not working
 */
let config = {
    // granadanet
    node_addr: 'http://granada.tz.functori.com',
    node_port: '80',
    usleep: 5000, // 5 seconds
    // sandbox
    //node_addr: 'http://127.0.0.1',
    //node_port: 18731,
    //usleep: 50, // 50 milliseconds

    contract_addr: 'KT19Y5bd2WSF9VHChTisjnu6RQVZfyQnL244',

    wait_for_block_x_levels: 10
}

// Prepare an instance of Tezos Toolkit to interact with the contract
const tezosKit = new TezosToolkit(config.node_addr + ':' + config.node_port);

// Prepare a Client instance to send RPCs
const client = new RpcClient(config.node_addr + ':' + config.node_port);

/** auxiliary function to set (or unset if sk = null) the signer */
function setSigner(sk) {
    tezosKit.setProvider({
        signer: (sk === null) ? null : new InMemorySigner(sk),
    });
}


/** dumb function that waits until operation, whose hash is given, is included in the blockchain */
let wait_inclusion = async function (opH, start_lvl) {
    var end_lvl = start_lvl + config.wait_for_block_x_levels;
    var authorized_failures = 10;
    let getBlock = async function () {
        var b = null;
        while (b === null) {
            try {
                b = await client.getBlock();
            } catch (error) {
                authorized_failures--;
                console.log(JSON.stringify(error));
                if (authorized_failures <= 0) {
                    process.exit(0);
                }
            }
        }
        return b;
    };
    let retro_inspection = async function (b) {
        var b = getBlock(b.header.predecessor);
        console.log(`Retro inspect level ${b.header.level} for operation ${opH}`);
        let found_op = b.operations[3].find(op => op.hash === opH);
        if (found_op !== undefined) {
            return { block: b, included: true, op: found_op }
        }
        if (b.header.level < start_lvl) {
            throw "Operation not found"
        }
        return await retro_inspection(b);
    };
    let aux = async function () {
        var b = await getBlock();
        var level = b.header.level;
        console.log(`Waiting inclusion ... block level is ${level}`);
        let found_op = b.operations[3].find(op => op.hash === opH);
        if (found_op !== undefined) {
            return { block: b, included: true, op: found_op }
        }
        if (end_lvl < level) {
            try {
                await retro_inspection(b);
            } catch (error) {
                console.log(error);
                return { block: b, included: false, op: null }
            }
        }
        while (level == b.header.level) {
            sleep.usleep(config.usleep);
            try {
                let b2 = await client.getBlock();
                level = b2.header.level;
            } catch (error) {
                authorized_failures--;
                console.log(JSON.stringify(error));
                if (authorized_failures <= 0) {
                    process.exit(0);
                }
            }
        }
        var res = await aux();
        return res;
    }
    if (opH == null) {
        throw ('Cannot monitor an operation whose hash is null');
    }
    return await aux()
}

var aragoWallets = {
    adminWallet: {
        sk: "edsk3MSgA4ivo4uFsY3Kh8B1dmykXz1JcibVrJmARQi8J62czoa67C",
        pk: "edpkup3kSxEHWvSh38R4YxCvTMaXM8H8aBRk1EGmyrSyA2yByfAVQu",
        pkh: "tz1NAMhMgsLnGXAfiPgcfyvsMpHzgiH6ndj7"
    },
    companyWallet: {
        sk: "edsk3kmMj1PRsWhcwjeFxGaKRuq5WexBarWmuqpTiqD3gmPGQhh9U4",
        pk: "edpkuRbbrp5fQWFhpYiZfmGi1fFeLkdPoT7L7XHqfrsRbZpZ84p8QS",
        pkh: "tz1Mfops8DCYfoUY26DciaTRtyMPBr4m2BbH"
    },
    managedWallet1: {
        sk: "edsk3eZ3quB3tuvy7RMJA4N5caXU9oYGEvzjKjjuw9NdN8k33EcF7J",
        pk: "edpktiHvek6TM2sdX7rQyzSHzEMR27U72aKV77neXRL5REGjRAsq3x",
        pkh: "tz1gq2QP8vTrqNaxCPEpkQZC5yey7sppuTyf"
    },
    managedWallet2: {
        sk: "edsk3sw5oxmWWsmvnqBEu8t251JZ65QU4P9uFD1aVeHW73ubPs8g7j",
        pk: "edpkuuiSnxDmTFvLWNph9puqMVUQ7YSH8nv7fNEgUFRXdYxFpkjsm1",
        pkh: "tz1aC1k1ZeN3pcXCtC6wUNkgy1Emu6XAzMga"
    },
    photographerManagedWallet: {
        sk: "edsk351m3kbNPdLiT6dMyhzpnAYzVzZDM6xdV7ZxHJhevQFhykRiw5",
        pk: "edpkvYBsBXBxH7HjmrasuquM2NWJT1ZKLHchyhbLYzxDLDeGQfGcpp",
        pkh: "tz1bNY71k5VBdJM8uMKa691ehRWNiTczxkYa"
    },
    externalWallet: {
        "sk": "edsk3WAiWm6d2e24DPmD1frjooKBcbZRghHmVEZWogE7VM6C4BF243",
        "pk": "edpkuyj2zZ3BTnSHHBfvc3Ww3u8dn86yAJxZ9AswhYdfCP65bKXd2v",
        "pkh": "tz1ZEHyTC8o75erHgpmNSQ4oPdaQ6NRADNsz"
    }
}

async function until_included(res, expected_err) {
    if (expected_err !== "" && res.error !== null && (expected_err === res.error || expected_err === res.error.message)) {
        console.log(`Test failed as expected: ${expected_err}`);
        return;
    }
    if (res.hash == null) {
        throw res
    }
    console.log(`Wait for inclusion of ${JSON.stringify(res)}`);
    let b = await client.getBlock();
    let inc = await wait_inclusion(res.hash, b.header.level);
    if (!inc.included) {
        throw `Operation not included after ${config.wait_for_block_x_levels} blocks`
    } else {
        console.log(`Operation ${res.hash} included`)
    }
}

const { MichelsonData, MichelsonType, packDataBytes } = require("@taquito/michel-codec")

function pack(
    data /* : MichelsonData*/,
    type /*: MichelsonType*/) /*: string*/ {
    return packDataBytes(data, type).bytes
}

async function simple_workflow() {
    try {

        console.log(`Config is: ${JSON.stringify(config)}`);

        // just here to reveal keys the first time

        var _r = await
            [aragoWallets.adminWallet, aragoWallets.companyWallet].reduce(
                async function (acc, w, index) {
                    acc = await acc;
                    console.log(`\n// Reveal wallet ${w.pkh}`);
                    setSigner(w.sk);
                    res = await nft.revealWallet(tezosKit);
                    return await until_included(res, "WalletAlreadyRevealed");
                },
                Promise.resolve())

        var s = await nft.storage(tezosKit, config.contract_addr);
        console.log(`Current storge is ${JSON.stringify(s)}`);

        console.log('\n// set aragoWallets.adminWallet as signer');
        setSigner(aragoWallets.adminWallet.sk);

        if (s.paused) {
            console.log('\n// unpause the contract');
            var res = await nft.unpause(tezosKit, config.contract_addr);
            await until_included(res, "");
        }
        console.log(`${s.company_wallet} vs ${aragoWallets.companyWallet.pkh}`)
        if (s.company_wallet !== aragoWallets.companyWallet.pkh) {
            console.log('\n// set aragoWallets.CompanyWallet as company wallet');
            var res = await nft.set_company_wallet(tezosKit, config.contract_addr, aragoWallets.companyWallet.pkh);
            await until_included(res, "");

            console.log('\n// set aragoWallets.companyWallet as signer');
            setSigner(aragoWallets.companyWallet.sk);

            console.log('\n// confirm aragoWallets.companyWallet as company wallet');
            var res = await nft.confirm_company_wallet(tezosKit, config.contract_addr);
            await until_included(res, "");
        }

        console.log('\n// Register managed wallets as managed');

        var b = await client.getBlock();
        var pair = { prim: 'Pair', args: [{ string: config.contract_addr }, { string: b.chain_id }] };
        var pairType = { prim: 'pair', args: [{ prim: 'address' }, { prim: 'chain_id' }] };
        var packedData = pack(pair, pairType);
        console.log(packedData);

        // all wallets to manage will sign a message off-chain with their private key
        var managedMessages = [];
        var _r = await
            [aragoWallets.managedWallet1, aragoWallets.managedWallet2, aragoWallets.photographerManagedWallet].reduce(
                async function (acc, w, index) {
                    acc = await acc;
                    console.log(`\n// Will Manage wallet ${w.pkh}`);
                    setSigner(w.sk);
                    let signature = await tezosKit.signer.sign(packedData);
                    console.log(`signature is ${JSON.stringify(signature.prefixSig)}\n`);
                    var p = { pk: w.pk, signed_msg: signature.prefixSig };
                    console.log(JSON.stringify(p));
                    managedMessages.push(p);
                },
                Promise.resolve())
        // company wallet will then set these wallets as managed with the signed messages
        setSigner(aragoWallets.companyWallet.sk);
        console.log(JSON.stringify(managedMessages));
        res = await nft.managed(tezosKit, config.contract_addr, managedMessages);
        await until_included(res, "");

        console.log('\n// Mint 8 tokens with their meta token/data');

        setSigner(aragoWallets.adminWallet.sk);
        var s = await nft.storage(tezosKit, config.contract_addr);
        let mid = parseInt(s.next_meta_token_id);
        let NB_TOKENS_PER_META = parseInt(s._NB_TOKENS_PER_META);
        let META_TOKEN_SHIFT = parseInt(s._META_TOKEN_SHIFT);

        assert(NB_TOKENS_PER_META = 8);
        let tid_base = META_TOKEN_SHIFT * mid
        let tok1 = tid_base + 1;
        let tok2 = tid_base + 2;
        let tok3 = tid_base + 3;
        let tok4 = tid_base + 4;
        let tok5 = tid_base + 5;
        let tok6 = tid_base + 6;
        let tok7 = tid_base + 7;
        let tok8 = tid_base + 8;

        var tokens = [
            { token_id: tok1, owner: aragoWallets.photographerManagedWallet.pkh },
            { token_id: tok2, owner: aragoWallets.companyWallet.pkh },
            { token_id: tok3, owner: aragoWallets.companyWallet.pkh },
            { token_id: tok4, owner: aragoWallets.companyWallet.pkh },
            { token_id: tok5, owner: aragoWallets.companyWallet.pkh },
            { token_id: tok6, owner: aragoWallets.companyWallet.pkh },
            { token_id: tok7, owner: aragoWallets.companyWallet.pkh },
            { token_id: tok8, owner: aragoWallets.companyWallet.pkh }
        ];

        // at each execution of these tests, we modify tokens metadata to include a fresh timestamp
        var now = Buffer.from(Date.now().toString(), 'utf8').toString('hex');
        var title = Buffer.from('Nocturno', 'utf8').toString('hex');
        var author = Buffer.from('Kristopher Grunert', 'utf8').toString('hex');

        var metadata = {
            "title": `${title}`,
            "created_at": `${now}`,
            "author": `${author}`,
            "sd_sha256sum": '03042cf8100db386818cee4ff0f2972431a62ed78edbd09ac08accfabbefd818',
            "hd_sha256sum": '52ff0394dde3f5c22aa54118acd4955c4f5f5d88064dc5b16898cc0508c6f2d8',
            "sd_link": '68747470733a2f2f62616679626569656d78663561626a776a62696b6f7a346d63336133646c613675616c336a736770647234636a72336f7a336576667961766877712e697066732e647765622e6c696e6b2f492f6d2f56696e63656e745f76616e5f476f67685f2d5f53656c662d506f7274726169745f2d5f476f6f676c655f4172745f50726f6a6563745f28343534303435292e6a7067',
            1: `01`, // put the data you want here
            2: `02`,
            3: `03`,
            4: `04`,
            5: `05`,
            6: `06`,
            7: `07`,
            8: `08`,
        }
        var mints = [
            {
                tokens: tokens,
                meta_token_id: mid,
                token_info: metadata
            }
        ]
        console.log(JSON.stringify(mints));
        res = await nft.mint(tezosKit, config.contract_addr, mints);
        await until_included(res, "");

        console.log('\n// Tokens transfer: 1 and 2 to managed1, 2 and 4 to managed 2, plus the photographer"s token. All these are signed by the company wallet');
        setSigner(aragoWallets.companyWallet.sk);
        var tr = [
            {
                from_: aragoWallets.companyWallet.pkh, // sender is really company wallet
                txs: [{ to_: aragoWallets.managedWallet1.pkh, token_id: tid_base + 2, amount: 1 },
                { to_: aragoWallets.managedWallet1.pkh, token_id: tid_base + 3, amount: 1 },
                { to_: aragoWallets.managedWallet2.pkh, token_id: tid_base + 4, amount: 1 },
                { to_: aragoWallets.managedWallet2.pkh, token_id: tid_base + 5, amount: 1 },
                ]
            },
            {
                from_: aragoWallets.photographerManagedWallet.pkh, // sent by company wallet on behalf of photographer
                txs: [{ to_: aragoWallets.externalWallet.pkh, token_id: tid_base + 1, amount: 1 }]
            }
        ];
        res = await nft.transfer(tezosKit, config.contract_addr, tr);
        await until_included(res, "");

        console.log('\n// Burn tokens id + 5, id + 6 and id + 7');
        setSigner(aragoWallets.adminWallet.sk);
        res = await nft.burn(tezosKit, config.contract_addr, [tid_base + 6, tid_base + 7, tid_base + 8]);
        await until_included(res, "");

        console.log('\n// Transfer back from external wallet to photographer wallet');
        setSigner(aragoWallets.externalWallet.sk);
        tr = [{ from_: aragoWallets.externalWallet.pkh, txs: [{ to_: aragoWallets.photographerManagedWallet.pkh, amount: 1, token_id: tid_base + 1 }] }]
        res = await nft.transfer(tezosKit, config.contract_addr, tr);
        await until_included(res, "");


        var secret = crypto.randomBytes(32).toString('hex'); // this is the secret
        var nonce = crypto.randomBytes(32).toString('hex'); // for more security, this is a nonce appended to the secret

        concat = `${secret}${nonce}` // concatenate secret and nonce
        const hash = crypto.createHash('sha256');
        hash.update(Buffer.from(concat, 'hex'));
        var hashed = hash.digest('hex'); // compute sha256(secret . nonce)

        console.log(`Secret is ${secret}, nonce is ${nonce}, commitment is ${hashed}`);
        // console.log (`echo ${concat} | xxd -r -p | shasum -a 256 -t`); how to concatenate and compute the secret in the shell

        console.log('\n// Make photographer"s token untransferable (for the demo)');
        setSigner(aragoWallets.adminWallet.sk);
        var param = [{ set_token_id: tid_base + 1, set_owner: aragoWallets.photographerManagedWallet.pkh, set_hash: hashed }];
        console.log(JSON.stringify(param));
        res = await nft.set_untransferable(tezosKit, config.contract_addr, param);
        await until_included(res, "");

        console.log('\n// Transfer from photographer wallet to external wallet should fail with "TOKEN_UNTRANSFERABLE"');
        setSigner(aragoWallets.photographerManagedWallet.sk);
        tr = [{ from_: aragoWallets.photographerManagedWallet.pkh, txs: [{ to_: aragoWallets.externalWallet.pkh, amount: 1, token_id: tid_base + 1 }] }]
        res = await nft.transfer(tezosKit, config.contract_addr, tr);
        await until_included(res, "TOKEN_UNTRANSFERABLE");

        console.log('\n// Make photographer"s token transferable (for the demo): bad secret');
        setSigner(aragoWallets.adminWallet.sk);
        var param = [{ unset_token_id: tid_base + 1, unset_owner: aragoWallets.photographerManagedWallet.pkh, unset_secret: '', unset_nonce: '' }];
        console.log(JSON.stringify(param));
        res = await nft.unset_untransferable(tezosKit, config.contract_addr, param);
        await until_included(res, "WRONG_REVEALED_SECRET");

        console.log('\n// Make photographer"s token transferable (for the demo): correct secret');
        setSigner(aragoWallets.adminWallet.sk);
        var param = [{ unset_token_id: tid_base + 1, unset_owner: aragoWallets.photographerManagedWallet.pkh, unset_secret: secret, unset_nonce: nonce }];
        console.log(JSON.stringify(param));
        res = await nft.unset_untransferable(tezosKit, config.contract_addr, param);
        await until_included(res, "");

        console.log('\n// Transfer from photographer wallet to external wallet should now succeed');
        setSigner(aragoWallets.photographerManagedWallet.sk);
        tr = [{ from_: aragoWallets.photographerManagedWallet.pkh, txs: [{ to_: aragoWallets.externalWallet.pkh, amount: 1, token_id: tid_base + 1 }] }]
        res = await nft.transfer(tezosKit, config.contract_addr, tr);
        await until_included(res, "");

        console.log('ended successfully');

    } catch (error) {
        console.log(error);
        console.log(JSON.stringify(error));
    }
}

simple_workflow();

