
const nft = require("./fa2_nft.js");
const cryptoKeys = require("./crypto.js");
const { TezosToolkit } = require('@taquito/taquito');
const { InMemorySigner, importKey } = require('@taquito/signer');
const assert = require('assert').strict;
const sleep = require('sleep');
const { RpcClient } = require('@taquito/rpc');

// ----

export interface Circ_List<T> {
    action: T;
    next: Circ_List<T>
}

/**
 * Three actions kinds that are managed by the injectors. Each of which are made of individual tasks.
 * For this simple version, individual tasks of each kind are coded items of a circular list. At each round/loop,
 * the injector will
 * - process tasks of the current item, and
 * - move to the next item (that will be processed at the next loop call)
 * Operations injected in each kind are signed by different keys:
 * - adminActions are signed by the admin's secret key
 * - cwalletActions are signed by the company wallet's key
 */
type adminActions = Circ_List<'Mint' | 'Burn'>
type cwalletActions = Circ_List<'Manage' | 'Transfer' | 'UpdateOperators'>


/**
 * This is the config
 */
export interface Config {
    kt1_address: string; // The KT1's address
    admin_secret_key: string; // store it securely
    cwallet_secret_key: string; // store it securely
    sleep_duration: number, // The amount of idle time beteween two successive injector rounds
    max_managed_ops_per_loop: number, // max operations of managed kind to process per round
    max_admin_ops_per_loop: number, // TODO refine this in term of bytes, not urgent
    max_company_wallet_ops_per_loop: number, // TODO refine this in term of bytes, not urgent
    fees_per_byte: number, // TODO, quite hard, not urgent
    confirmation_blocks: number, // after inclusion in the main chain, the number of blocks of top of an operation to consider it's confirmed
    max_operations_ttl: number, // cannot be changed currently in Taquito, put here for documentation. If an operation is not injected after this tll, it will never be
    next_admin_action: adminActions,
    next_cwallet_action: cwalletActions
}

/**
 * Simple auxiliary function to move forward by one item in the given circular list
 * @param cl the circular list
 * @returns the new circular list
 */
function step_forward<A>(cl: Circ_List<A>): Circ_List<A> {
    switch (cl.next) {
        case null:
            console.log('Circ list is illformed');
            process.exit(1);
        default:
            return cl.next
    }
}

/**
 * This is the skeleton function to inject the 'admin' operations kind
 */
async function make_admin_operations(config: Config): Promise<void> {
    // console.log (config.next_admin_action); // inspect the circular list
    let action = config.next_admin_action.action;
    console.log(`make_admin_operations: action is ${action}`);
    config.next_admin_action = step_forward(config.next_admin_action);
    switch (action) {
        case 'Mint':
            /**
             * - Select pending mint requests from the DB
             * - encode them as the mint entrypoint's parameter
             * - Call the mint entrypoint. Pay attention to tokens and meta tokens IDs and to metadata
             * - Save the operation's hash into DB
             */
            console.log('TODO');
            break;
        case 'Burn':
            /**
             * Select burn requests from DB:
             * - For requests made by managed wallets, make sure they are burnable, if yes, the admin can burn them
             * - Encode the burn list as a parameter of the burn entrypoint
             * - Call the entrypoints and save the operation hash into DB (call signed by the admin)
             */
            console.log('TODO');
            break;
        default:
            console.log('Failure: case is not exhaustive');
            process.exit(1);
    }
}

/**
 * This is the skeleton function to inject the 'cwallet' operations kind
 */
async function make_company_wallet_operations(config: Config): Promise<void> {
    // console.log (config.next_cwallet_action); // inspect the circular list
    let action = config.next_cwallet_action.action
    console.log(`make_company_wallet_operations: action is ${action}`);
    config.next_cwallet_action = step_forward(config.next_cwallet_action);
    switch (action) {
        case 'Manage':
            /**
             * - generate a key pairs
             * - sign a message with the private key offline
             * - send the signed message via the managed entrypoint by the companyWallet
             */
            console.log('TODO');
            break;
        case 'Transfer':
            /**
             * - Select transfer requests from DB
             * - encode the list of transfers as a parameter of the 'transfer' entrypoint
             * - sources is either company wallet or managed wallets, in both case, the company wallet can sign the operation
             * - inject the operation and save its hash into DB
             */
            console.log('TODO');
            break;
        case 'UpdateOperators':
            /**
             * quite similar to the transfer above
             */
            console.log('TODO');
            break;
        default:
            console.log('Failure: case is not exhaustive');
            process.exit(1);
    }
}

/**
 * This is the global functions for operations injection
 */
async function inject_new_operations(config: Config): Promise<void> {
    console.log('inject_new_operations');
    await make_admin_operations(config);
    await make_company_wallet_operations(config);
}

/**
 * This is the main function for injected operations monitoring.
 * => Define auxiliary functions to monitor each time of each kind separately
 */
async function monitor_injected_operations(config: Config): Promise<void> {
    console.log('monitor_injected_operations: TODO');
    /* Here, check the result of recently injected operations using
        - the injection levels
        - the operations hash
        - the content of crawler's table (find by operations)
    -> and update operations statuses from:
       - 'injected', to
       - 'included', to
       - 'confirmed', or to
       - 'failed', if the injection or inclusion failed for some reason

    In the crawler, an operation is included when the 'main' field is true (this flag can be reverted if chain backtracks).
    It's confirmed if the `confirmed` field is true (the crawler doesn't revert it even in case of chain backtrack)
    Note that the 'failed' case can also happen without having an operation hash (eg. operation injection failed)

    -> operations that have to be tracked can be classified to these categories:
        admin_operations:
        - Mint
        - Burn
        company_wallet_operations
        - Manage
        - Transfer
        - UpdateOperators
    */
}

/**
 * This is the main injector loop, that repeatedly:
 * - monitors recent injected operations
 * - inject new operation
 * - sleep for amount of time
 * @param cpt : counts the number of round (for debug purpose)
 * @param config : the configuration
 */
async function injector_loop(cpt: number, config: Config): Promise<void> {
    console.log(`\nInjector loop number ${cpt}`);
    console.log('+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+');
    await monitor_injected_operations(config);
    await inject_new_operations(config);
    console.log(`Round ${cpt} finished. Sleeping ${config.sleep_duration} seconds ...`);
    sleep.sleep(config.sleep_duration)
    await injector_loop(cpt + 1, config);
}

/**
 * Initializing the config before calling the injector's loop
 */

// A circular list for admin actions
var admin_action_burn = function (): adminActions {
    return { action: 'Burn', next: admin_action_mint }
}
var admin_action_mint: adminActions = { action: 'Mint', next: admin_action_burn() }
admin_action_mint.next.next = admin_action_mint; // force 'admin_action_mint' to be circular

// A circular list for company wallet actions
var cwallet_action_updateOperators = function (): cwalletActions {
    return { action: 'UpdateOperators', next: cwallet_action_manage }
}
var cwallet_action_transfer = function (): cwalletActions {
    return { action: 'Transfer', next: cwallet_action_updateOperators() }
}
var cwallet_action_manage: cwalletActions = { action: 'Manage', next: cwallet_action_transfer() }
cwallet_action_manage.next.next.next = cwallet_action_manage; // force the list to be circular

let config: Config = {
    kt1_address: 'TODO',
    admin_secret_key: 'TODO', // store it securely
    cwallet_secret_key: 'TODO', // store it securely
    sleep_duration: 5, // 45 seconds
    max_managed_ops_per_loop: 100,
    max_admin_ops_per_loop: 25, // TODO refine this in term of bytes, not urgent
    max_company_wallet_ops_per_loop: 25, // TODO refine this in term of bytes, not urgent
    fees_per_byte: 0, // TODO, quite hard, not urgent
    confirmation_blocks: 6,
    max_operations_ttl: 120, // cannot be changed currently in Taquito, put here for documentation
    next_admin_action: admin_action_mint,
    next_cwallet_action: cwallet_action_manage
}

injector_loop(1, config);
