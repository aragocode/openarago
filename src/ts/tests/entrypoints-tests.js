/****************************************************************************************************************
This file's goal is twofold:
- Show how to use different entrypoints of the NFT smart contract, via the JS/Typescript interface library
- Test different corner cases of the smart contract's entrypoints, and of the JS/Typescript library itself

Note that:
- Replaying all the tests takes some time
- If a test fails and it is not expected to, it may be because the operation is not injected/included for some
reasons (the granadanet node is down, no baker included the operation, ...)
****************************************************************************************************************/

const nft = require("../dist/fa2_nft.js");

const { TezosToolkit } = require('@taquito/taquito');
const { InMemorySigner, importKey } = require('@taquito/signer');

const assert = require('assert').strict; // used for assertions in the tests
const sleep = require('sleep');
const { RpcClient } = require('@taquito/rpc');

/**
 * Global config. Make sure to:
 * - modify the smart contract address if a new one is originated
 * - set another node address if the given one is not working
 */
let config = {
    // granadanet
    //node_addr: 'http://granada.tz.functori.com',
    //node_port: '80',
    //usleep: 5000, // 5 seconds
    // sandbox
    node_addr: 'http://127.0.0.1',
    node_port: 18731,
    usleep: 50, // 50 milliseconds

    contract_addr: 'KT1WJ788YDU4xhphCFcNADMQv2op6HDp4CKx',

    // these two simple contracts are intended to be used as callbacks when testing the "balance_of" entrypoint.
    // The first one expect balance_of to return 0, and the second one is used when it returns 1 for a token
    callback_zero_balance_of: 'X',
    callback_one_balance_of: 'X',

    // Whether we are testing individual calls via the batched mode or mode.
    // The test driver below starts by testing in normal mode (ie. calling individual entrypoints functions).
    // Then it tests the "batch" function to increase library testing
    batched_mode: false,

    airdrop_amount: 0.3
}

// Prepare an instance of Tezos Toolkit to interact with the contract
const tezosKit = new TezosToolkit(config.node_addr + ':' + config.node_port);

// Prepare a Client instance to send RPCs
const client = new RpcClient(config.node_addr + ':' + config.node_port);

/**
 * These are the list of wallets used during the tests. Make sure that:
 * - they are funded
 * - the first one in the list is the admin wallet
 * - the second one is the company wallet, when it's different from admin wallet
 * - the third one is a managed wallet
 */
var wallets = [
    // at cell zero, we have always admin, at cell 1, we have company wallet when it's different from admin
    {
        "sk": "edsk3MSgA4ivo4uFsY3Kh8B1dmykXz1JcibVrJmARQi8J62czoa67C",
        "pk": "edpkup3kSxEHWvSh38R4YxCvTMaXM8H8aBRk1EGmyrSyA2yByfAVQu",
        "pkh": "tz1NAMhMgsLnGXAfiPgcfyvsMpHzgiH6ndj7"
    },
    {
        "sk": "edsk3kmMj1PRsWhcwjeFxGaKRuq5WexBarWmuqpTiqD3gmPGQhh9U4",
        "pk": "edpkuRbbrp5fQWFhpYiZfmGi1fFeLkdPoT7L7XHqfrsRbZpZ84p8QS",
        "pkh": "tz1Mfops8DCYfoUY26DciaTRtyMPBr4m2BbH"
    },
    {
        "sk": "edsk3eZ3quB3tuvy7RMJA4N5caXU9oYGEvzjKjjuw9NdN8k33EcF7J",
        "pk": "edpktiHvek6TM2sdX7rQyzSHzEMR27U72aKV77neXRL5REGjRAsq3x",
        "pkh": "tz1gq2QP8vTrqNaxCPEpkQZC5yey7sppuTyf"
    },
    {
        "sk": "edsk34mDx6KgFjcZ87NrHmF4yPGedHaHHivYYrcN7L4KCFPd2atWxG",
        "pk": "edpkv4XbfsE72HivoXH5sLxuyeTrJUVo9V5ayjiP4TrbFcYBmtcygn",
        "pkh": "tz1Ks7cfurXiHFniZuJhLQg8rQdxf8W82pCy"
    }
]

var cpt = 0;

/** auxiliary function to set (or unset if sk = null) the signer */
function setSigner(sk) {
    tezosKit.setProvider({
        signer: (sk === null) ? null : new InMemorySigner(sk),
    });
}


// BEGIN: This is for debug
/** The code below is intended to get the lines in the nodeJS source code of calls' locations */
Object.defineProperty(global, '__stack', {
    get: function () {
        var orig = Error.prepareStackTrace;
        Error.prepareStackTrace = function (_, stack) {
            return stack;
        };
        var err = new Error;
        Error.captureStackTrace(err, arguments.callee);
        var stack = err.stack;
        Error.prepareStackTrace = orig;
        return stack;
    }
});

Object.defineProperty(global, '__line', {
    get: function () {
        return __stack[1].getLineNumber();
    }
});
// END: This is for debug

/**
 * Small util class to manage code location. Used to precisely track locations of tests,
 * in particular in case auxiliary functions are used to factorize some tests
 */
function withLoc(loc, locs) {
    var out = locs === null || locs === undefined ? new Array() : locs.slice();
    out.push(loc);
    return out
}

/** dumb function that waits until operation, whose hash is given, is included in the blockchain */
let wait_inclusion = async function (opH, end_lvl) {
    var authorized_failures = 10;
    let aux = async function () {
        var b = null;
        var level = null;
        while (b === null) {
            try {
                b = await client.getBlock();
                level = b.header.level;
            } catch (error) {
                authorized_failures--;
                console.log(JSON.stringify(error));
                if (authorized_failures <= 0) {
                    process.exit(0);
                }
            }
        }
        let found_op = b.operations[3].find(op => op.hash === opH);
        if (found_op !== undefined) {
            return { block: b, included: true, op: found_op }
        }
        if (end_lvl < level) {
            return { block: b, included: false, op: null }
        }
        while (level == b.header.level) {
            sleep.usleep(config.usleep);
            try {
                let b2 = await client.getBlock();
                level = b2.header.level;
            } catch (error) {
                authorized_failures--;
                console.log(JSON.stringify(error));
                if (authorized_failures <= 0) {
                    process.exit(0);
                }
            }
        }
        var res = await aux();
        return res;
    }
    if (opH == null) {
        throw ('Cannot monitor an operation whose hash is null');
    }
    return await aux()
}

/**
 * Generic function to call entrypoints using the JS library, and to report on their statuses
 * @param {*} line: the line from which the test is executed (for debug only)
 * @param {*} caller_id: the wallet ID of the caller from the wallets list above
 * @param {*} entrypoint_name : the entrypoint's name (for debug only)
 * @param {*} msg : a message associated with the test (for debug only)
 * @param {*} entrypoint : the entrypoint to be called
 * @param {*} params : the parameters that should be provided when calling the entrypoint
 * @param {*} expected_err : empty string in case of a test that should succeed, the error message otherwise
 */
async function do_call(locs, caller_id, entrypoint_name, msg, entrypoint, params, expected_err) {
    cpt++;
    let line = JSON.stringify(locs);
    // retry injection X times, to catchs where the op is legit but not injected/included
    let retry_call = async function (func, param, n) {
        try {
            let block = await client.getBlock();
            console.log(`Current block level is ${block.header.level}`);
            let res = await func(tezosKit, config.contract_addr, param);
            if (res.hash === null) { // injection failed for some reason, retry
                throw res.error;
            } else {
                console.log(`Operation hash is ${res.hash}`)
            }
            let nb = 20;
            let inc = await wait_inclusion(res.hash, block.header.level + nb); // we wait 4 blocks for the op to be included
            if (!inc.included) {
                throw `Operation not included after ${nb} blocks`
            } else {
                console.log(`Operation ${res.hash} included`)
            }
            assert(inc.op.hash == res.hash);
            return res.hash;
        } catch (error) {
            if (n <= 0 || error.message === expected_err) {
                throw error
            } else {
                console.log(`Test ${cpt}: Unexpected error. ${n} attempts left.`)
                console.log(JSON.stringify(error));
                sleep.sleep(10);
                return await retry_call(func, param, n - 1)
            }
        }
    };
    try {
        let caller = wallets[caller_id];
        console.log(`\n>## TEST ${cpt} (line ${line}): Calling entrypoint "${entrypoint_name}" with wallet ${caller.pkh}`);
        console.log(msg);
        console.log(`Parameter is ${JSON.stringify(params)}`);
        setSigner(caller.sk);
        var x = null
        if (config.batched_mode && entrypoint_name !== "batch") {
            // test normal calls as batched calls
            console.log("Re-route to BATCHING mode ... ");
            let bparams = [{ kind: entrypoint_name, param: params }];
            x = await retry_call(nft.batch, bparams, 4);
        } else {
            x = await retry_call(entrypoint, params, 4);
        }
        console.log("Operation injected with hash: " + JSON.stringify(x));
        // unset the signer, for safety
        setSigner(null);
        if (expected_err !== "") {
            console.log(`The test ${cpt}, line ${line} is supposed to fail, but it succeeded. Exiting ...`);
            process.exit(2);
        } else {
            console.log(`##> Test ${cpt}, line ${line} succeeded as expected.`);
        }
    } catch (e) {
        // unset the signer, for safety
        setSigner(null);
        let got_err_msg = e.message;
        if (expected_err === "" || expected_err !== got_err_msg) {
            console.log('An error occurred while injecting the operation.\n');
            console.log(JSON.stringify(e));
            console.log('\nMake sure the caller is authorized on this entrypoint, the wallet is funded, the KT1 address is correct, ....');
            console.log(`Exiting (was test ${cpt}, line ${line})...`);
            process.exit(1);
        } else {
            console.log(`##> Test ${cpt}, line ${line} failed as expected (Got error: ${got_err_msg})`);
        }
    }
}

//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// pause(bool)
//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
let pause_tests = async function (locs) {
    var name = "";
    var msg = "";
    name = "pause";

    // params are ignored when invoking the entrypoint interfaces, because two functions pause and unpause are provided by the lib
    // but these params are useful/used in batched mode
    let pause_param = true;
    let unpause_param = false;
    msg = "Expected to fail because a bad admin key is given";
    await do_call(withLoc(__line, locs), 1, name, msg, nft.pause, pause_param, "NOT_AN_ADMIN");

    msg = "Expected to succeed";
    await do_call(withLoc(__line, locs), 0, name, msg, nft.pause, pause_param, "");

    msg = "Expected to fail because a bad admin key is given";
    await do_call(withLoc(__line, locs), 1, name, msg, nft.unpause, unpause_param, "NOT_AN_ADMIN");

    msg = "Expected to succeed";
    await do_call(withLoc(__line, locs), 0, name, msg, nft.unpause, unpause_param, "");
}

//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// set_admin(address) // confirm_admin(unit)
//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
let swap_admin = async function (locs) {
    var name = "";
    var msg = "";
    name = "set_admin";
    msg = "Expected to fail because a non admin cannot set a new admin";
    await do_call(withLoc(__line, locs), 2, name, msg, nft.set_admin, wallets[2].pkh, "NOT_AN_ADMIN");

    name = "set_admin";
    msg = "Expected to succeed";
    await do_call(withLoc(__line, locs), 0, name, msg, nft.set_admin, wallets[3].pkh, "");

    name = "set_admin";
    msg = "Expected to succeed";
    await do_call(withLoc(__line, locs), 0, name, msg, nft.set_admin, wallets[1].pkh, "");

    name = "confirm_admin";
    msg = "Expected to fail. This old candidate admin cannot confirm anymore";
    await do_call(withLoc(__line, locs), 3, name, msg, nft.confirm_admin, null, "NOT_A_PENDING_ADMIN");

    name = "confirm_admin";
    msg = "Expected to fail. Different from latest admin set for confirmation";
    await do_call(withLoc(__line, locs), 0, name, msg, nft.confirm_admin, null, "NOT_A_PENDING_ADMIN");

    name = "confirm_admin";
    msg = "Expected to succeed";
    await do_call(withLoc(__line, locs), 1, name, msg, nft.confirm_admin, null, "");

    name = "confirm_admin";
    msg = "Expected to fail: No pending admin";
    await do_call(withLoc(__line, locs), 1, name, msg, nft.confirm_admin, null, "NO_PENDING_ADMIN");

    // !!! ADMIN NOW CHANGED !!! Put the new admin in wallets[0]
    old_admin = wallets[0];
    new_admin = wallets[1];
    wallets[0] = new_admin;
    wallets[1] = old_admin;
}


//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// set_company_wallet(address) // confirm_company_wallet(unit)
//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
let swap_company_wallet = async function (locs) {
    var name = "";
    var msg = "";

    // 0 - TMP: put admin as company wallet
    name = "set_company_wallet";
    msg = "Expected to succeed";
    await do_call(withLoc(__line, locs), 0, name, msg, nft.set_company_wallet, wallets[0].pkh, "");

    name = "confirm_company_wallet";
    msg = "Expected to succeed";
    await do_call(withLoc(__line, locs), 0, name, msg, nft.confirm_company_wallet, null, "");

    // 1 - Tests: put (back) wallets[1] as company wallet, with some failing cases
    name = "set_company_wallet";
    msg = "Expected to fail because a non admin cannot set a new company wallet";
    await do_call(withLoc(__line, locs), 2, name, msg, nft.set_company_wallet, wallets[2].pkh, "NOT_AN_ADMIN");

    name = "set_company_wallet";
    msg = "Expected to succeed";
    await do_call(withLoc(__line, locs), 0, name, msg, nft.set_company_wallet, wallets[3].pkh, "");

    msg = "Expected to succeed";
    await do_call(withLoc(__line, locs), 0, name, msg, nft.set_company_wallet, wallets[1].pkh, "");

    name = "confirm_company_wallet";
    msg = "Expected to fail. This old candidate company wallet cannot confirm anymore";
    await do_call(withLoc(__line, locs), 3, name, msg, nft.confirm_company_wallet, null, "NOT_A_PENDING_COMPANY_WALLET");

    msg = "Expected to fail. Different from latest company wallet set for confirmation";
    await do_call(withLoc(__line, locs), 0, name, msg, nft.confirm_company_wallet, null, "NOT_A_PENDING_COMPANY_WALLET");

    msg = "Expected to succeed";
    await do_call(withLoc(__line, locs), 1, name, msg, nft.confirm_company_wallet, null, "");

    msg = "Expected to fail: no pending company wallet";
    await do_call(withLoc(__line, locs), 1, name, msg, nft.confirm_company_wallet, null, "NO_PENDING_COMPANY_WALLET");

    name = "set_company_wallet";
    msg = "Expected to fail: current company wallet cannot set a new CW candidate";
    await do_call(withLoc(__line, locs), 1, name, msg, nft.set_company_wallet, wallets[3].pkh, "NOT_AN_ADMIN");

    // wallets[1] is now the company wallet
}


//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// mint
//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// aux function for mint
let patch_tokens_ids = async function (pl, patch_meta_id, with_metadata) {
    let s = await nft.storage(tezosKit, config.contract_addr);
    console.log(JSON.stringify(s));
    var cpt = s.next_token_id;
    let l = JSON.parse(JSON.stringify(pl));
    l.map(function (p) {
        p.token_id = cpt;
        cpt++;
        if (!with_metadata) {
            p.metadata = null;
        }
        switch (patch_meta_id) {
            case 'ignore':
                break;
            case 'next':
                p.meta_token_id = s.next_meta_token_id;
                break;
            case 'last':
                p.meta_token_id = parseInt(s.next_meta_token_id) - 1;
                break;
            default:
                p.meta_token_id = parseInt(patch_meta_id)
                break;
        }
        return p
    });
    return l;
};

// tests
let mint = async function (locs, BURNABLE_FLAG, uncapped_tests) {
    let init_storage = await nft.storage(tezosKit, config.contract_addr); // initial contract storage
    var name = "";
    var msg = "";
    let filter = 2 ** 32;
    // at each execution of these tests, we modify tokens metadata to include a fresh timestamp
    var now = Date.now() % filter + filter; // is a valid hex
    name = "mint_tokens";
    var meta_token_info =
        (BURNABLE_FLAG === undefined || BURNABLE_FLAG === null)
            ? { "TestTimeStamp": `${now}` }
            : { "TestTimeStamp": `${now}`, "burnable": BURNABLE_FLAG };

    let DUMMY = Number.MAX_SAFE_INTEGER; // (quasi) impossible to reach this limit with tests. We use it as DUMMY ID

    // - not an admin
    var meta_token_param = { token_info: meta_token_info, max_supply: "4" };
    var d_mint_params = [{ owner: wallets[2].pkh, token_id: DUMMY, meta_token_id: DUMMY, metadata: meta_token_param }]; // dummy token id

    let par1 = await patch_tokens_ids(d_mint_params, 'next', true);
    msg = "Expected to fail: non-admin accounts cannot mint";
    await do_call(withLoc(__line, locs), 1, name, msg, nft.mint, par1, "NOT_AN_ADMIN");

    // - with wrong token ID: WRONG_TOKEN_ID
    let par2 = d_mint_params; // wrong IDS
    msg = "Expected to fail: WRONG_TOKEN_ID";
    await do_call(withLoc(__line, locs), 0, name, msg, nft.mint, par2, "WRONG_TOKEN_ID");

    // - mint with a supposed existing meta token: meta token id wrong, inexistent
    let par3 = await patch_tokens_ids(d_mint_params, 'ignore', true); // meta token ID is wrong
    msg = "Expected to fail: WRONG_META_TOKEN_ID";
    await do_call(withLoc(__line, locs), 0, name, msg, nft.mint, par3, "WRONG_META_TOKEN_ID");

    // - without metadata when required: NO_METADATA_INFO
    let par4 = await patch_tokens_ids(d_mint_params, 'next', false); // new meta token, but no metadata given
    msg = "Expected to fail: NO_METADATA_INFO";
    await do_call(withLoc(__line, locs), 0, name, msg, nft.mint, par4, "NO_METADATA_INFO");

    // - mint with a new meta token: capped
    let par5 = await patch_tokens_ids(d_mint_params, 'next', true); // basis for new mints
    msg = "Expected to succeed (Minting one token for wallets[2])";
    // We mint the first token of the meta-token (whose max supply is 4)
    await do_call(withLoc(__line, locs), 0, name, msg, nft.mint, par5, "");

    var d_mint_params = [
        { owner: wallets[2].pkh, token_id: DUMMY, meta_token_id: DUMMY, metadata: meta_token_param },
        { owner: wallets[3].pkh, token_id: DUMMY, meta_token_id: DUMMY, metadata: meta_token_param }]; // dummy token id

    // - mint with an existing meta token: provide metadata, but unrequired: UNREQUIRED_TOKEN_METADATA
    let par6 = await patch_tokens_ids(d_mint_params, 'last', true); // mint existing token, with metadata
    msg = "Expected to fail: minting with an existing meta token, but providing metadata";
    await do_call(withLoc(__line, locs), 0, name, msg, nft.mint, par6, "UNREQUIRED_TOKEN_METADATA");

    // - mint with an existing meta token (capped): without providing metadata, basis for re-mint
    let par7 = await patch_tokens_ids(d_mint_params, 'last', false); // mint existing token, without metadata
    msg = "Expected to succeed (Minting two tokens for wallets[2] and 3)";
    await do_call(withLoc(__line, locs), 0, name, msg, nft.mint, par7, "");

    // attempt to mint more tokens
    var d_mint_params = [
        { owner: wallets[1].pkh, token_id: DUMMY, meta_token_id: DUMMY, metadata: meta_token_param },
        { owner: wallets[2].pkh, token_id: DUMMY, meta_token_id: DUMMY, metadata: meta_token_param }]; // dummy token id
    // - mint with an existing meta token (capped): MAX_SUPPLY_EXCEEDED
    let par8 = await patch_tokens_ids(d_mint_params, 'last', false); // mint existing token, without metadata
    // trying to mint two more tokens to have 5 tokens minted, while supply's max is 4 -> failure expected
    msg = "Expected to fail ((Minting another token: Cannot have 5 tokens for a curr_supply = max supply = 4)";
    await do_call(withLoc(__line, locs), 0, name, msg, nft.mint, par8, "MAX_SUPPLY_EXCEEDED");

    // mint more tokens without exceeding max supply
    var d_mint_params = [
        { owner: wallets[2].pkh, token_id: DUMMY, meta_token_id: DUMMY, metadata: meta_token_param }]; // dummy token id
    // - mint with an existing meta token (capped): MAX SUPPLY REACHED
    let par9 = await patch_tokens_ids(d_mint_params, 'last', false); // mint existing token, without metadata
    msg = "Expected to succeed (Minting the fourth token for wallets[2])";
    // minting the 4th token out of 4. Meta token is identified by ID
    await do_call(withLoc(__line, locs), 0, name, msg, nft.mint, par9, ""); //----------XXXXX

    // cannot mint more ...
    msg = "Expected to fail (Token ID already used ...)";
    await do_call(withLoc(__line, locs), 0, name, msg, nft.mint, par9, "WRONG_TOKEN_ID");

    let par10 = await patch_tokens_ids(d_mint_params, 'last', false); // mint existing token, without metadata
    msg = "Expected to fail (Max supply already reached)";
    await do_call(withLoc(__line, locs), 0, name, msg, nft.mint, par10, "MAX_SUPPLY_EXCEEDED");


    // - mint with a new meta token: MAX_SUPPLY_EXCEEDED
    var meta_token_param = { token_info: meta_token_info, max_supply: "0" };
    var d_mint_params = [{ owner: wallets[2].pkh, token_id: DUMMY, meta_token_id: DUMMY, metadata: meta_token_param }]; // dummy token id
    let par11 = await patch_tokens_ids(d_mint_params, 'next', true); // basis for new mints
    msg = "Expected to fail (Cannot mint with a new meta token if max_supply is 0)";
    await do_call(withLoc(__line, locs), 0, name, msg, nft.mint, par11, "MAX_SUPPLY_EXCEEDED");

    if (uncapped_tests) {

        // - mint with a new meta token: uncapped

        var meta_token_param = { token_info: meta_token_info, max_supply: null };

        var d_mint_params = [{ owner: wallets[2].pkh, token_id: DUMMY, meta_token_id: DUMMY, metadata: meta_token_param },
        { owner: wallets[2].pkh, token_id: DUMMY, meta_token_id: DUMMY, metadata: null },
        { owner: wallets[3].pkh, token_id: DUMMY, meta_token_id: DUMMY, metadata: meta_token_param },
        { owner: wallets[1].pkh, token_id: DUMMY, meta_token_id: DUMMY, metadata: null },
        { owner: wallets[0].pkh, token_id: DUMMY, meta_token_id: DUMMY, metadata: null },
        { owner: wallets[2].pkh, token_id: DUMMY, meta_token_id: DUMMY, metadata: null }
        ];
        let par12 = await patch_tokens_ids(d_mint_params, 'next', true); // basis for new mints
        msg = "Expected to fail metadata for the same token provided twice above";
        // metadata for the same/existing token provided twice in the d_mint_params variable above
        await do_call(withLoc(__line, locs), 0, name, msg, nft.mint, par12, "UNREQUIRED_TOKEN_METADATA");

        var d_mint_params = [{ owner: wallets[2].pkh, token_id: DUMMY, meta_token_id: DUMMY, metadata: meta_token_param },
        { owner: wallets[2].pkh, token_id: DUMMY, meta_token_id: DUMMY, metadata: null },
        { owner: wallets[3].pkh, token_id: DUMMY, meta_token_id: DUMMY, metadata: null },
        { owner: wallets[1].pkh, token_id: DUMMY, meta_token_id: DUMMY, metadata: null },
        { owner: wallets[0].pkh, token_id: DUMMY, meta_token_id: DUMMY, metadata: null },
        { owner: wallets[2].pkh, token_id: DUMMY, meta_token_id: DUMMY, metadata: null }
        ];
        let par13 = await patch_tokens_ids(d_mint_params, 'next', true); // basis for new mints
        msg = "Expected to succeed: mint 6 tokens attached to a newly created uncapped meta token";
        await do_call(withLoc(__line, locs), 0, name, msg, nft.mint, par13, "");

        //    - mint with an existing meta token: uncapped

        var d_mint_params = [{ owner: wallets[2].pkh, token_id: DUMMY, meta_token_id: DUMMY, metadata: meta_token_param },
        { owner: wallets[0].pkh, token_id: DUMMY, meta_token_id: DUMMY, metadata: null },
        { owner: wallets[2].pkh, token_id: DUMMY, meta_token_id: DUMMY, metadata: null },
        { owner: wallets[0].pkh, token_id: DUMMY, meta_token_id: DUMMY, metadata: null },
        { owner: wallets[1].pkh, token_id: DUMMY, meta_token_id: DUMMY, metadata: null },
        { owner: wallets[1].pkh, token_id: DUMMY, meta_token_id: DUMMY, metadata: null },
        { owner: wallets[1].pkh, token_id: DUMMY, meta_token_id: DUMMY, metadata: null }
        ];
        let par14 = await patch_tokens_ids(d_mint_params, 'last', false); // basis for new mints
        msg = "Expected to succeed: mint 7 additional tokens attached to an existing uncapped meta token";
        await do_call(withLoc(__line, locs), 0, name, msg, nft.mint, par14, "");

    }

    let storage2 = await nft.storage(tezosKit, config.contract_addr); // contract storage after minting

    assert(storage2.next_meta_token_id = init_storage.next_meta_token_id + 1);
    assert(storage2.next_token_id = init_storage.next_token_id + 4);
};


//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// burn
//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
let burn = async function (locs) {
    let storage = await nft.storage(tezosKit, config.contract_addr); // initial contract storage
    var name = "";
    var msg = "";
    name = "burn_tokens";

    // This function assumes that tokens for 4 new meta tokens have been minted just before the call
    let meta_1 = storage.next_meta_token_id - 4; // burnable
    let meta_2 = storage.next_meta_token_id - 3; // not burnable
    let meta_3 = storage.next_meta_token_id - 2; // burnable
    let meta_4 = storage.next_meta_token_id - 1; // not burnable
    let last_token_id = storage.next_token_id - 1;
    let r0 = last_token_id - 4 * 4; // 4 meta_tokens, with 4 tokens for each
    // current distribution
    //  meta_1            |#| meta_2            |#| meta_3              |#| meta_4
    //  r0 | +1 | +2 | +3 |#| +4 | +5 | +6 | +7 |#| +8 | +9 | +10 | +11 |#| r12 | +13 | +14 | +15 |

    var params = [];


    params = [r0 + 1];
    msg = "Expected to fail. wallets[3]. cannot burn a burnable token it doesn't own";
    await do_call(withLoc(__line, locs), 3, name, msg, nft.burn, params, "NOT_AN_ADMIN_OR_OWNER");

    msg = "Expected to succeed. wallets[2]. can burn its own burnable token";
    await do_call(withLoc(__line, locs), 2, name, msg, nft.burn, params, "");

    params = [r0 + 4 + 1];
    msg = "Expected to fail. wallets[2]. Cannot burn a non-burnable token it owns";
    await do_call(withLoc(__line, locs), 2, name, msg, nft.burn, params, "NOT_BURNABLE");

    params = [r0 + 3, r0 + 4 + 2, r0 + 8 + 1, r0 + 12];
    msg = "Expected succeed. Admin can burn any token";
    await do_call(withLoc(__line, locs), 0, name, msg, nft.burn, params, "");

    msg = "Expected fail. Cannot burn a non-existing token. Even for admin";
    await do_call(withLoc(__line, locs), 0, name, msg, nft.burn, params, "FA2_TOKEN_UNDEFINED");

    // current distribution
    //  meta_1            |#| meta_2            |#| meta_3              |#| meta_4
    //  r0 | x  | +2 | x  |#| +4 | +5 | x | +7 |#| +8 | x  | +10 | +11 |#| rx | +13 | +14 | +15 |


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    name = "mint_tokens";
    msg = "Expected to succeed (Minting in a previously full meta-token)";
    // We mint the first token of the meta-token (whose max supply is 4)
    let DUMMY = Number.MAX_SAFE_INTEGER; // (quasi) impossible to reach this limit with tests. We use it as DUMMY ID

    var store = await nft.storage(tezosKit, config.contract_addr);

    // mint for meta token = meta_1
    var d_mint_params = [{ owner: wallets[3].pkh, token_id: DUMMY, meta_token_id: DUMMY, metadata: null }];

    let par1 = await patch_tokens_ids(d_mint_params, meta_1, false); // mint new token associated to meta_1
    msg = "Expected to succeed (Minting the third token (again) token for wallets[3])";
    await do_call(withLoc(__line, locs), 0, name, msg, nft.mint, par1, "");

    let par2 = await patch_tokens_ids(d_mint_params, meta_1, false); // mint new token associated to meta_1
    msg = "Expected to succeed (Minting the fourth token (again) token for wallets[3])";
    await do_call(withLoc(__line, locs), 0, name, msg, nft.mint, par2, "");

    let par3 = await patch_tokens_ids(d_mint_params, meta_1, false); // mint new token associated to meta_1
    msg = "Expected to fail (Cannot mint more than max supply)";
    await do_call(withLoc(__line, locs), 0, name, msg, nft.mint, par3, "MAX_SUPPLY_EXCEEDED");

    store = await nft.storage(tezosKit, config.contract_addr);

    // mint for meta token = meta_2
    var d_mint_params = [{ owner: wallets[1].pkh, token_id: DUMMY, meta_token_id: DUMMY, metadata: null }];

    let par4 = await patch_tokens_ids(d_mint_params, meta_2, false); // mint new token associated to meta_2
    msg = "Expected to fail: NOT ADMIN";
    await do_call(withLoc(__line, locs), 1, name, msg, nft.mint, par4, "NOT_AN_ADMIN");

    msg = "Expected to succeed (Minting the fourth token (again) token for wallets[1])";
    await do_call(withLoc(__line, locs), 0, name, msg, nft.mint, par4, ""); // same par4, but call by admin

    let par5 = await patch_tokens_ids(d_mint_params, meta_2, false); // mint new token associated to meta_2
    msg = "Expected to fail (Cannot mint more than max supply)";
    await do_call(withLoc(__line, locs), 0, name, msg, nft.mint, par5, "MAX_SUPPLY_EXCEEDED");
};


//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// managed
//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
let managed = async function (locs) {
    var name = "managed"
    var params = null
    params = true;
    msg = "Expected to succeed. wallets[3] calls/becomes managed";
    await do_call(withLoc(__line, locs), 3, name, msg, nft.managed, params, "");

    msg = "Expected to succeed. wallets[3] already managed";
    await do_call(withLoc(__line, locs), 3, name, msg, nft.managed, params, "");

    params = false;
    msg = "Expected to succeed. wallets[3] becomes unmanaged";
    await do_call(withLoc(__line, locs), 3, name, msg, nft.managed, params, "");

    msg = "Expected to succeed. wallets[3] already unmanaged";
    await do_call(withLoc(__line, locs), 3, name, msg, nft.managed, params, "");

    params = true;
    msg = "Expected to succeed. wallets[2] (third key starting from 0) calls/becomes managed";
    await do_call(withLoc(__line, locs), 2, name, msg, nft.managed, params, "");

};

//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// update_operators
//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
let update_operators = async function (locs, removeOps) {
    let storage = await nft.storage(tezosKit, config.contract_addr); // initial contract storage
    let last_token_id = storage.next_token_id - 1;
    var name = null;
    var params = null;
    name = "update_operators";

    params = [[{ owner: wallets[2].pkh, operator: wallets[3].pkh, token_id: last_token_id }, true]];
    msg = "Expected to fail: owner not managed and different from caller";
    await do_call(withLoc(__line, locs), 3, name, msg, nft.update_operators, params, "FA2_NOT_OWNER");

    msg = "Expected to fail: admin cannot update operators on behalf of owners";
    await do_call(withLoc(__line, locs), 0, name, msg, nft.update_operators, params, "FA2_NOT_OWNER");

    msg = "Expected to succeed: owner = caller ";
    await do_call(withLoc(__line, locs), 2, name, msg, nft.update_operators, params, "");

    msg = "Expected to succeed: owner = caller (already added)";
    await do_call(withLoc(__line, locs), 2, name, msg, nft.update_operators, params, "");

    msg = "Expected to succeed: owner <> caller but caller = CW and owner is managed";
    await do_call(withLoc(__line, locs), 1, name, msg, nft.update_operators, params, "");

    msg = "Expected to succeed: owner <> caller but caller = CW and owner is managed (already added)";
    await do_call(withLoc(__line, locs), 1, name, msg, nft.update_operators, params, "");

    params = [[{ owner: wallets[3].pkh, operator: wallets[1].pkh, token_id: last_token_id + 10 }, true]];
    msg = "Expected to succeed: owner = caller, on a token whose ID doesn't exit (yet)";
    await do_call(withLoc(__line, locs), 3, name, msg, nft.update_operators, params, "");

    msg = "Expected to fail: company wallet cannot update operators on behalf of non-managed accounts (3)";
    await do_call(withLoc(__line, locs), 1, name, msg, nft.update_operators, params, "FA2_NOT_OWNER");

    /// attempts to remove operators
    if (removeOps) {
        params = [[{ owner: wallets[2].pkh, operator: wallets[3].pkh, token_id: last_token_id }, false]];
        msg = "Expected to fail: owner not managed and different from caller";
        await do_call(withLoc(__line, locs), 3, name, msg, nft.update_operators, params, "FA2_NOT_OWNER");

        msg = "Expected to fail: admin cannot update operators on behalf of owners";
        await do_call(withLoc(__line, locs), 0, name, msg, nft.update_operators, params, "FA2_NOT_OWNER");

        msg = "Expected to succeed: owner = caller ";
        await do_call(withLoc(__line, locs), 2, name, msg, nft.update_operators, params, "");

        msg = "Expected to succeed: owner = caller (removing an already removed)";
        await do_call(withLoc(__line, locs), 2, name, msg, nft.update_operators, params, "");

        msg = "Expected to succeed: owner <> caller but caller = CW and owner is managed";
        await do_call(withLoc(__line, locs), 1, name, msg, nft.update_operators, params, "");

        msg = "Expected to succeed: owner <> caller but caller = CW and owner is managed (adding an already removed)";
        await do_call(withLoc(__line, locs), 1, name, msg, nft.update_operators, params, "");

        params = [[{ owner: wallets[3].pkh, operator: wallets[1].pkh, token_id: last_token_id + 10 }, false]];
        msg = "Expected to succeed: owner = caller, on a token whose ID doesn't exit (yet)";
        await do_call(withLoc(__line, locs), 3, name, msg, nft.update_operators, params, "");

        msg = "Expected to fail: company wallet cannot update operators on behalf of non-managed accounts (3)";
        await do_call(withLoc(__line, locs), 1, name, msg, nft.update_operators, params, "FA2_NOT_OWNER");
    }
};


//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// transfer
//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
/* helper */
let mk_transfer_param = function (pfrom_, pto_, token, amount) {
    let from_ = wallets[pfrom_].pkh;
    let to_ = wallets[pto_].pkh;
    let txs = [{ to_: to_, token_id: token, amount: amount }];
    return [{ from_: from_, txs: txs }]
}

let transfer = async function (locs) {
    var name = "transfer"
    var params, p1, p2 = [];

    // we first re-mint, to be sure who owns what
    await mint(withLoc(__line), null); // not burnable

    let storage = await nft.storage(tezosKit, config.contract_addr); // initial contract storage
    let next_token_id = storage.next_token_id;
    /* Distribution is supposed to be as follows:
                     | admin      | c-wallet     | managed      | external
                     | w0         | w1           | w2           | w3
    -----------------|------------|--------------|--------------|----------------
    tokens wrt N     |            |              | N-4, N-3, N-1| N-2
    -----------------|------------|--------------|--------------|----------------
    */

    // warm up transfers tokens from owners wallets to themselves
    let auto_transfers = async function (msg, balance, expected_err) {
        for (delta = 1; delta <= 4; delta++) {
            let w = delta === 2 ? 3 : 2; // only w3 owns token N-2. Others belong to w2
            let token = next_token_id - delta;
            params = mk_transfer_param(w, w, token, balance);
            await do_call(withLoc(__line, locs), w, name, msg, nft.transfer, params, expected_err);
        }
    };
    await auto_transfers("Expected to succeed", 1, "");
    await auto_transfers("Expected to succeed", 0, "");
    await auto_transfers("Expected to fail", 2, "FA2_INSUFFICIENT_BALANCE");

    // admin or w3 cannot transfer two w2's tokens to w1
    let tr_from_others_without_permission = async function (src, expected_err) {
        for (w = 0; w <= 3; w = w + 3) {
            p1 = mk_transfer_param(src, 1, next_token_id - 4, 1);
            p2 = mk_transfer_param(src, 1, next_token_id - 1, 1);
            params = p1.concat(p2);
            msg = "Expected to fail: " + expected_err;
            await do_call(withLoc(__line, locs), w, name, msg, nft.transfer, params, expected_err);
        }
    };
    await tr_from_others_without_permission(3, "FA2_INSUFFICIENT_BALANCE"); // from w = 3. Rather FA2_NOT_OWNER ?
    await tr_from_others_without_permission(2, "FA2_NOT_OPERATOR"); // from w = 2, ie real owner

    // transferring a token that does not exit (yet).
    params = mk_transfer_param(3, 2, next_token_id + 1, 1);
    msg = "Expected to fail: the token does not exist";
    await do_call(withLoc(__line, locs), 3, name, msg, nft.transfer, params, "FA2_TOKEN_UNDEFINED");

    // company wallet w1 can transfer from w2 (managed) to w0

    p1 = mk_transfer_param(2, 0, next_token_id - 4, 1);
    p2 = mk_transfer_param(2, 0, next_token_id - 1, 1);
    params = p1.concat(p2);
    msg = "Expected to succeed: w2 is managed by w1";
    await do_call(withLoc(__line, locs), 1, name, msg, nft.transfer, params, "");

    msg = "Expected to fail: tokens already transferred";
    await do_call(withLoc(__line, locs), 1, name, msg, nft.transfer, params, "FA2_INSUFFICIENT_BALANCE");

    // w0 transfers back tokens from w0 to w2
    p1 = mk_transfer_param(0, 2, next_token_id - 4, 1);
    p2 = mk_transfer_param(0, 2, next_token_id - 1, 1);
    params = p2.concat(p1);
    msg = "Expected to succeed: direct transfers of owned tokens";
    await do_call(withLoc(__line, locs), 0, name, msg, nft.transfer, params, "");

    // company wallet w1 cannot transfer from a non managed wallet w3)
    params = mk_transfer_param(3, 0, next_token_id - 2, 1);
    msg = "Expected to fail: w3 is not managed by w1";
    await do_call(withLoc(__line, locs), 1, name, msg, nft.transfer, params, "FA2_NOT_OPERATOR");

    // w3 cannot transfer "next_token_id - 1" from w2, because it's not an operator
    params = mk_transfer_param(2, 3, next_token_id - 1, 1);
    msg = "Expected to fail: w3 cannot transfer 'next_token_id - 1' from w2";
    await do_call(withLoc(__line, locs), 3, name, msg, nft.transfer, params, "FA2_NOT_OPERATOR");

    // but if company wallet w1 sets w3 as operator of w2 on "next_token_id - 1" (w2 is managed) ...
    var name_uop = "update_operators";
    var param_uop = [[{ owner: wallets[2].pkh, operator: wallets[3].pkh, token_id: next_token_id - 1 }, true]];
    var msg_uop = "Expected to succeed"
    await do_call(withLoc(__line, locs), 1, name_uop, msg_uop, nft.update_operators, param_uop, "");

    // w3 can transfer "next_token_id - 1" from w2
    params = mk_transfer_param(2, 3, next_token_id - 1, 1);
    msg = "Expected to succeed";
    await do_call(withLoc(__line, locs), 3, name, msg, nft.transfer, params, "");

    // but w3 cannot transfer other tokens of w2. like eg. "next_token_id - 3"
    params = mk_transfer_param(2, 3, next_token_id - 3, 1);
    msg = "Expected to fail: w3 cannot transfer 'next_token_id - 3' from w2";
    await do_call(withLoc(__line, locs), 3, name, msg, nft.transfer, params, "FA2_NOT_OPERATOR");

    // w3 transfers back "next_token_id - 1" to w2
    params = mk_transfer_param(3, 2, next_token_id - 1, 1);
    msg = "Expected to succeed";
    await do_call(withLoc(__line, locs), 3, name, msg, nft.transfer, params, "");

    // update_operators: w1 removes transfer permission from w3 on w2's token "next_token_id - 1"
    var name_uop = "update_operators";
    var param_uop = [[{ owner: wallets[2].pkh, operator: wallets[3].pkh, token_id: next_token_id - 1 }, false]];
    var msg_uop = "Expected to succeed"
    await do_call(withLoc(__line, locs), 1, name_uop, msg_uop, nft.update_operators, param_uop, "");

    // again, w3 cannot transfer "next_token_id - 1" from w2 (without authorization)
    params = mk_transfer_param(2, 3, next_token_id - 1, 1);
    msg = "Expected to fail: w3 cannot transfer 'next_token_id - 1' from w2 (permission removed)";
    await do_call(withLoc(__line, locs), 3, name, msg, nft.transfer, params, "FA2_NOT_OPERATOR");

    // regular update_operators / permission from w3 to w0
    param_uop = [[{ owner: wallets[3].pkh, operator: wallets[0].pkh, token_id: next_token_id - 2 }, true]];
    msg_uop = "Expected to succeed"
    await do_call(withLoc(__line, locs), 3, name_uop, msg_uop, nft.update_operators, param_uop, "");

    // No, w0 transfers the token from w3 to w0
    params = mk_transfer_param(3, 0, next_token_id - 2, 1);
    msg = "Expected to succeed";
    await do_call(withLoc(__line, locs), 0, name, msg, nft.transfer, params, "");

    // removing transfer permission from w3 to w0
    param_uop = [[{ owner: wallets[3].pkh, operator: wallets[0].pkh, token_id: next_token_id - 2 }, false]];
    msg_uop = "Expected to succeed"
    await do_call(withLoc(__line, locs), 3, name_uop, msg_uop, nft.update_operators, param_uop, "");

    // w2 is not managed by w1 anymore
    var man_name = "managed";
    var man_params = false;
    var man_msg = "Expected to succeed. wallets[2] becomes unmanaged";
    await do_call(withLoc(__line, locs), 2, man_name, man_msg, nft.managed, man_params, "");

    // company wallet w1 can NOT transfer from w2 (previously managed) to w0 ANYMORE
    p1 = mk_transfer_param(2, 0, next_token_id - 4, 1);
    p2 = mk_transfer_param(2, 0, next_token_id - 1, 1);
    params = p1.concat(p2);
    msg = "Expected to fail: w2 is NOT managed by w1 ANYMORE";
    await do_call(withLoc(__line, locs), 1, name, msg, nft.transfer, params, "FA2_NOT_OPERATOR");

    // company wallet w1 can NOT grant permissions on w2 (previously managed) ANYMORE
    name_uop = "update_operators";
    param_uop = [[{ owner: wallets[2].pkh, operator: wallets[3].pkh, token_id: next_token_id - 1 }, true]];
    msg_uop = "Expected to fail: w2 is NOT managed by w1 ANYMORE";
    await do_call(withLoc(__line, locs), 1, name_uop, msg_uop, nft.update_operators, param_uop, "FA2_NOT_OWNER");

};

//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// balance_of
//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
let balance_of = async function (locs) {
    var name = null
    var msg = null
    var params, p1, p2 = [];

    let storage = await nft.storage(tezosKit, config.contract_addr); // initial contract storage

    name = "balance_of";
    // request balance of 0 tokens
    params = { requests: [], callback: config.callback_zero_balance_of };
    msg = "Expected to succeed: ask for balance of 0 tokens";
    await do_call(withLoc(__line, locs), 1, name, msg, nft.balance_of, params, "");

    params = { requests: [], callback: config.callback_one_balance_of };
    msg = "Expected to succeed: ask for balance of 0 tokens";
    await do_call(withLoc(__line, locs), 1, name, msg, nft.balance_of, params, "");

    // request balance of non existing token: storage.next_token_id doesn't exist (yet)
    params = {
        requests: [{ owner: wallets[2].pkh, token_id: storage.next_token_id }],
        callback: config.callback_one_balance_of
    };
    msg = "Expected to fail: token storage.next_token_id does not exit";
    await do_call(withLoc(__line, locs), 1, name, msg, nft.balance_of, params, "FA2_TOKEN_UNDEFINED");

    params = {
        requests: [{ owner: wallets[2].pkh, token_id: storage.next_token_id }],
        callback: config.callback_zero_balance_of
    };
    msg = "Expected to fail: token storage.next_token_id does not exit";
    await do_call(withLoc(__line, locs), 1, name, msg, nft.balance_of, params, "FA2_TOKEN_UNDEFINED");

    // last minted token "tok" belongs to w2
    let tok = storage.next_token_id - 1;

    // request balance of an existing token that belongs to the announced owner
    params = {
        requests: [{ owner: wallets[2].pkh, token_id: tok }],
        callback: config.callback_zero_balance_of
    };
    msg = "Expected to fail: balance should be one, not zero";
    await do_call(withLoc(__line, locs), 1, name, msg, nft.balance_of, params, "CALLBACK:UNEXPECTED_BALANCE_RESULT");

    params = {
        requests: [{ owner: wallets[2].pkh, token_id: tok }],
        callback: config.callback_one_balance_of
    };
    msg = "Expected to succeed";
    await do_call(withLoc(__line, locs), 1, name, msg, nft.balance_of, params, "");

    // request balance of an existing token that does not belong to the announced owner
    params = {
        requests: [{ owner: wallets[0].pkh, token_id: tok }],
        callback: config.callback_one_balance_of
    };
    msg = "Expected to fail: balance should be zero, not one";
    await do_call(withLoc(__line, locs), 1, name, msg, nft.balance_of, params, "CALLBACK:UNEXPECTED_BALANCE_RESULT");

    params = {
        requests: [{ owner: wallets[0].pkh, token_id: tok }],
        callback: config.callback_zero_balance_of
    };
    msg = "Expected to succeed";
    await do_call(withLoc(__line, locs), 1, name, msg, nft.balance_of, params, "");
}


//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// token_metadata
//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
let token_metadata = async function (locs) {

    var name = null
    var msg = null
    var params, p1, p2 = [];

    /*
      The object below is the JSON version of the Michelson function in
      src/utils-contracts/metadata-continuation/continuation-burnable.tez, which is itself obtained from
      the compilation of the LIGO function in
      src/utils-contracts/metadata-continuation/continuation-burnable.mligo.
      => It is the continuation used to test token_metadata entrypoint: it succeeds if the token's metadata
      has the "burnable" entry, and fails with 'CONTINUATION:NOT_BURNABLE', otherwise
    */
    let continuation_function =
        [{ prim: 'CDR' },
        {
            prim: 'PUSH',
            args:
                [{ prim: 'string' },
                { string: 'burnable' }]
        },
        { prim: 'GET' },
        {
            prim: 'IF_NONE',
            args:
                [[{
                    prim: 'PUSH',
                    args:
                        [{ prim: 'string' },
                        {
                            string:
                                'CONTINUATION:NOT_BURNABLE'
                        }]
                },
                { prim: 'FAILWITH' }],
                [{ prim: 'DROP' },
                {
                    prim: 'PUSH',
                    args:
                        [{ prim: 'unit' },
                        { prim: 'Unit' }]
                }]]
        }];

    let storage = await nft.storage(tezosKit, config.contract_addr); // initial contract storage

    name = "token_metadata";
    // request balance of 0 tokens
    params = { token_id: 0, handler: continuation_function };
    msg = "Expected to succeed. The 4 first tokens in the tests driver are burnable";
    await do_call(withLoc(__line, locs), 1, name, msg, nft.token_metadata, params, "");

    params = { token_id: 3, handler: continuation_function };
    msg = "Expected to succeed. The 4 first tokens in the tests driver are burnable";
    await do_call(withLoc(__line, locs), 1, name, msg, nft.token_metadata, params, "");

    params = { token_id: 4, handler: continuation_function };
    msg = "Expected to succeed. The 4 next minted tokens [4 - 7] in the tests driver are NOT burnable";
    await do_call(withLoc(__line, locs), 1, name, msg, nft.token_metadata, params, "CONTINUATION:NOT_BURNABLE");

    params = { token_id: 5, handler: continuation_function };
    msg = "Expected to succeed. The 4 next minted tokens [4 - 7] in the tests driver are NOT burnable";
    await do_call(withLoc(__line, locs), 1, name, msg, nft.token_metadata, params, "CONTINUATION:NOT_BURNABLE");

    params = { token_id: 7, handler: continuation_function };
    msg = "Expected to succeed. The 4 next minted tokens [4 - 7] in the tests driver are NOT burnable";
    await do_call(withLoc(__line, locs), 1, name, msg, nft.token_metadata, params, "CONTINUATION:NOT_BURNABLE");
};

//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// batching calls:
// This is not an entrypoint, but a way to inject several transactions from the same
// source in the same op.
//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
let batch = async function (locs) {
    var name = "batch"
    var params, p1, p2, p3 = null

    // mint to be sure about who owns what
    await mint(withLoc(__line), "00"); // burnable

    let storage = await nft.storage(tezosKit, config.contract_addr); // initial contract storage

    // w2 will
    // - register itself as managed
    // - set w0 as operator on "last_token_id - 2"
    // - transfer a token to w1

    p1 = { kind: 'managed', param: true }
    p2 = { kind: 'update_operators', param: [[{ owner: wallets[2].pkh, operator: wallets[0].pkh, token_id: storage.next_token_id - 4 }, true]] }
    p3 = { kind: 'transfer', param: mk_transfer_param(2, 1, storage.next_token_id - 3, 1) }
    msg = "Expected to succeed. Batch from w2";
    await do_call(withLoc(__line, locs), 2, name, msg, nft.batch, [p1, p2, p3], "");

};

/**
 * This functions tests the following workflow:
 * - generate a fresh wallet tz1Man
 * - transfer 0.9 xtz to it from the admin wallet
 * - reveal the public key of tz1Man
 * - set the tz1Man as managed in the smart contract
 */
const cryptoKeys = require("../dist/crypto.js");

async function new_managed_wallet_workflow() {
    let checkInclusion = function (inc, inj_res, name) {
        if (inc.included) {
            console.log(`${name}: Op ${inj_res.hash} included`);
        } else {
            console.log(`${name}: Op ${inj_res.hash} NOT INCLUDED ... exiting!`);
            process.exit(1);
        }
    };
    let checkInjection = function (inj_res) {
        if (inj_res.hash == null) {
            console.log(`Operation injection failed with: ${JSON.stringify(inj_res.error)}`);
            process.exit(1);
        }
    }
    try {
        var res = null;
        var block = null;
        var inc = null;
        let amount = config.airdrop_amount;
        console.log('\nGenerate a fresh wallet tz1Man');
        let tzMan = await cryptoKeys.generate_keys();
        console.log(`New keys are: ${JSON.stringify(tzMan)}`);

        console.log(`\nTransfer ${amount} xtz to ${tzMan.pkh} from the admin wallet`);
        block = await client.getBlock();
        setSigner(wallets[0].sk);
        res = await nft.transfer_xtz(tezosKit, [tzMan.pkh], amount);
        checkInjection(res);
        inc = await wait_inclusion(res.hash, block.header.level + 4);
        checkInclusion(inc, res, "Transfer from admin");

        console.log('\nReveal the public key of tz1Man');
        block = await client.getBlock();
        setSigner(tzMan.sk);
        res = await nft.revealWallet(tezosKit);
        checkInjection(res);
        inc = await wait_inclusion(res.hash, block.header.level + 4);
        checkInclusion(inc, res, "Reveal public key");

        console.log('\nSet the tz1Man as managed in the smart contract');
        block = await client.getBlock();
        setSigner(tzMan.sk);
        res = await nft.managed(tezosKit, config.contract_addr, true);
        checkInjection(res);
        inc = await wait_inclusion(res.hash, block.header.level + 4);
        checkInclusion(inc, res, "Set tz1Man as managed");

    } catch (error) {
        console.log('Error: ' + JSON.stringify(error) + error);
        process.exit(1);
    }
}

/**
 * The tests driver
 */
async function tests_driver() {

    console.log('TODO: Adapt');

    return;

    console.log(`Config is: ${JSON.stringify(config)}`);

    let all_tests = async function () {
        // tests for pause/unpause
        await pause_tests(withLoc(__line));

        // Do these tests once the contract is unpaused
        await new_managed_wallet_workflow();

        // tests for set_admin/confirm_admin
        await swap_admin(withLoc(__line));
        await pause_tests(withLoc(__line)); // replay "pause" tests with new admin
        await swap_admin(withLoc(__line)); // restore initial configuration for admin

        // tests for set_company_wallet / confirm_company_wallet
        await swap_company_wallet(withLoc(__line)); //

        // tests for mint
        process.exit(0);
        await mint(withLoc(__line), "00", false); // burnable
        await mint(withLoc(__line), null, true); // not burnable + with tests for uncapped supply
        await mint(withLoc(__line), "00", false); // burnable
        await mint(withLoc(__line), null, false); // not burnable
        await mint(withLoc(__line), "00", false); // burnable
        await mint(withLoc(__line), null, false); // not burnable

        // tests for burn
        await burn(withLoc(__line)); //

        // tests for managed
        await managed(withLoc(__line)); //

        // tests for update_operators. Start by re-minting to be sure about who owns what.
        await update_operators(withLoc(__line), true); //

        // transfer entrypoint
        await transfer(withLoc(__line)); //

        // balance_of entrypoint
        await balance_of(withLoc(__line)); //

        // token_metadata entrypoint
        await token_metadata(withLoc(__line)); //

        // test batching of some transactions
        await batch(withLoc(__line));

    };

    console.log("\nTesting in normal (non batched) mode");
    config.batched_mode = false

    await all_tests();

    console.log("\nTesting in batched mode");
    config.batched_mode = true
    await all_tests();

    console.log('All tests successfully executed');

}

tests_driver();
