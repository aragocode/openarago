open Mligo
include Admin
include Fa2_errors

type mint_param = {
  token_id : token_id;
  owner : address;
} [@@comb]

type mint_meta_param = {
  meta_token_id : meta_token_id;
  token_info: meta_token_info;
  tokens: mint_param list
} [@@comb]

type set_untransferable_param = {
  set_token_id: token_id;
  set_owner: address;
  set_hash: bytes
} [@@comb]

type unset_untransferable_param = {
  unset_token_id: token_id;
  unset_owner: address;
  unset_secret: bytes;
  unset_nonce: bytes;
} [@@comb]


(* `token_manager` entry points *)
type token_manager =
  | Mint_tokens of mint_meta_param list
  | Burn_tokens of token_id list
  | Set_untransferable of set_untransferable_param list
  | Unset_untransferable of unset_untransferable_param list
[@@comb]

let mint_token ((s, next_token_id), p  : (storage * token_id) * mint_param) : storage *  token_id =
  let token_id = p.token_id in
  if token_id <> next_token_id then
    (failwith "WRONG_TOKEN_ID" : storage * token_id)
  else
    { s with
      ledger = Big_map.add token_id p.owner s.ledger;
    },  next_token_id + 1n

let mint_meta_token (s, mints : storage * mint_meta_param) : storage =
  if List.length mints.tokens <> s._NB_TOKENS_PER_META then
    (failwith "WRONG_NB_TOKENS_PER_META_TOKEN" : storage)
  else
    let meta_token_id = mints.meta_token_id in
    let token_id_base = mk_token_id_base s meta_token_id in
    if meta_token_id <> s.next_meta_token_id then
      (failwith "WRONG_META_TOKEN_ID" : storage)
    else
      let s = {
        s with
        meta_token_info = Big_map.add meta_token_id mints.token_info s.meta_token_info;
        meta_token_supply = Big_map.add meta_token_id s._NB_TOKENS_PER_META s.meta_token_supply;
        next_meta_token_id = meta_token_id + 1;
      }
      in
      let acc = (s, token_id_base + 1n) in
      let s, _cpt = List.fold mint_token mints.tokens acc in
      s

let mint_tokens (mints : mint_meta_param list) (s : storage) : storage =
  match mints with
  | [] -> (failwith "EMPTY_LIST" : storage)
  | _ -> List.fold mint_meta_token mints s

let burn_token (s, token_id : storage * token_id) : storage =
  let () =
    if Big_map.mem token_id s.ledger then ()
    else (failwith fa2_token_undefined : unit)
  in
  let meta_token_id = get_meta_token_id s token_id in
  let supply =
    match Big_map.find_opt meta_token_id s.meta_token_supply with
    | None -> (failwith "UNKNOWN_META_TOKEN_ID" : token_amount)
    | Some x -> x in
  let supply = match is_nat (supply - 1n) with
    | None -> (failwith "INVARIANT_BROKEN_1" : token_amount)
    | Some c -> c in
  { s with
    ledger = Big_map.remove token_id s.ledger;
    meta_token_supply =
      Big_map.add meta_token_id supply s.meta_token_supply;
  }

let burn_tokens (tokens : token_id list) (s : storage) =
  match tokens with
  | [] -> (failwith "EMPTY_LIST" : storage)
  | _ -> List.fold burn_token tokens s

let set_untransferable (pl : set_untransferable_param list) (s: storage) : storage =
  (* We can set a token as untransferable even before minting it for its owner *)
  List.fold
    (fun (s, p : storage * set_untransferable_param) ->
       let key = (p.set_token_id, p.set_owner) in
       match Big_map.find_opt key s.untransferable with
       | Some _ -> (failwith "HASHED_SECRET_ALREADY_SET" : storage)
       | None -> {s with untransferable = Big_map.add key p.set_hash s.untransferable}
    ) pl s

let unset_untransferable (pl : unset_untransferable_param list) (s: storage) : storage =
  List.fold
    (fun (s, p : storage * unset_untransferable_param) ->
       if (Tezos.sender <> s.admin && Tezos.sender <> p.unset_owner) then
         (failwith "NOT_AN_ADMIN_OR_OWNER" : storage)
       else
         let key = (p.unset_token_id, p.unset_owner) in
         match Big_map.find_opt key s.untransferable with
         | None -> (failwith "NOT_UNTRANSFERABLE" : storage) (*transferable or inexistent *)
         | Some committed_h ->
           let computed_h = Crypto.sha256 (Bytes.concat p.unset_secret p.unset_nonce) in
           if committed_h <> computed_h then
             (failwith "WRONG_REVEALED_SECRET" : storage)
           else
             {s with untransferable = Big_map.remove key s.untransferable}
    ) pl s


let token_manager (param, s : token_manager * storage) : (operation list) * storage =
  let s = match param with
    | Mint_tokens p ->
      let () = fail_if_not_admin s in
      mint_tokens p s
    | Burn_tokens p ->
      let () = fail_if_not_admin s in
      burn_tokens p s
    | Set_untransferable pl ->
      let () = fail_if_not_admin s in
      set_untransferable pl s
    | Unset_untransferable pl ->
      unset_untransferable pl s
  in
  ([] : operation list), s
