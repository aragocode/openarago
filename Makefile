DB=functori_nft_db
CRAWLORI_ROOT=vendors/crawlori

-include Makefile.config

all: build

build:
	@CRAWLORI_NO_UPDATE=true PGDATABASE=$(DB) PGCUSTOM_CONVERTERS_CONFIG=$(CRAWLORI_ROOT)/src/pg/converters dune build src/crawler/update.exe
	@CRAWLORI_NO_UPDATE=true PGDATABASE=$(DB) PGCUSTOM_CONVERTERS_CONFIG=$(CRAWLORI_ROOT)/src/pg/converters dune build

clean:
	@dune clean
	@rm -rf src/ts/node_modules
	@rm -f src/ts/package*.json
	@rm -rf src/ts/dist
	@rm -f _build/default/src/crawler/.functori_nft_db_witness

ts-deps:
	@sudo npm i -g typescript
	@npm --prefix src/ts --no-audit --no-fund i @taquito/signer hacl-wasm

ts:
	@tsc -p src/ts/tsconfig.json

node-deps:
	@cd src/ts/ && npm install .

opam-switch:
	@opam switch create . 4.12.0 --no-install

submodule:
	@git submodule init && git submodule update

config: submodule
	@echo "DB=$(DB)" > $(CRAWLORI_ROOT)/Makefile.config
	@echo "CRAWLORI_ROOT=$(CRAWLORI_ROOT)" >> $(CRAWLORI_ROOT)/Makefile.config

crawlori-deps: config
	@eval $$(opam env)
	@make -C $(CRAWLORI_ROOT) pg-deps

build-contracts:
	@dune build src/contract

build-deps: crawlori-deps
	@opam install . -y --deps-only --locked
	@eval $$(opam env)
	sudo $(OPAM_SWITCH_PREFIX)/bin/ligo_install

drop-db: clean
	@dropdb $(DB)
	@rm -f _build/default/src/crawler/.functori_nft_db_witness

uninstall: clean
	@opam switch remove . -y
