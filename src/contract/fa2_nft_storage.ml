open Mligo
include Fa2_nft_asset

let (metadata : (string, bytes) big_map) =
  Big_map.literal
  [
    "name",
    (*    "Arago Studio", *)
    0x417261676f2053747564696fh
    ;
    "symbol",
    (* "ARAGO", *)
    0x415241474fh
    ;
    "description",
    (* "Arago is building a new digital market for premium art photography", *)
    0x417261676f206973206275696c64696e672061206e6577206469676974616c206d61726b657420666f72207072656d69756d206172742070686f746f677261706879h
    ;
    "imageUri",
    (* "https://arago.studio/logo-nft.png", *)
    0x68747470733a2f2f617261676f2e73747564696f2f6c6f676f2d6e66742e706e67h
    ;
    "homepage",
    (* "https://arago.studio/", *)
    0x68747470733a2f2f617261676f2e73747564696f2fh
    ;
  ]

let store : storage = {
  admin = ("tz1QLu6EDPuAJ46EEnRzRQS8tT85NtG4kRTC" : address);
  company_wallet = ("tz1QRVx7S2k8nrdLEkjrFNt436ck66Ye5uik" : address);
  pending_admin = (None : address option);
  pending_company_wallet = (None : address option);
  paused = true;
  ledger = (Big_map.empty : (token_id, address) big_map);
  operators = (Big_map.empty : operator_storage);
  managed = (Big_map.empty : managed_storage);
  next_meta_token_id = 1;
  _NB_TOKENS_PER_META = 8n;
  _META_TOKEN_SHIFT =  1000;
  meta_token_info = (Big_map.empty : (meta_token_id, meta_token_info) big_map);
  meta_token_supply = (Big_map.empty : (meta_token_id, token_amount) big_map);
  untransferable = (Big_map.empty : ((token_id * address), hashed_secret) big_map);
  metadata = metadata
}
