type t = (string, bytes) map

type s = [@layout:comb] {
  token_id : nat;
  token_info : t
}

let main (x:s) : unit =
  match Map.find_opt "burnable" x.token_info with
  | None -> (failwith "CONTINUATION:NOT_BURNABLE" : unit)
  | Some _ -> ()
