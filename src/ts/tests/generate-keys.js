const crypto = require("../dist/crypto.js");

async function generate_keys() {
    let x = await crypto.generate_keys();
    console.log("\ngenerated keys are: " + JSON.stringify(x));
}

generate_keys()
