open Proto

type transfer_destination = {
  tr_destination: string;
  tr_token_id: int64;
  tr_amount: int64;
}

type transfer = {
  tr_source: string;
  tr_txs: transfer_destination list;
}

type operator_param = {
  op_owner : string;
  op_operator : string;
  op_token_id: int64;
  op_add: bool;
}

type mint_param = {
  mint_owner: string;
  mint_token_id: int64;
  mint_meta_token_id: int64;
  meta_info: (string * string) list;
} [@@deriving encoding]

type address = string

type entrypoints =
  | Operator_updates of operator_param list
  | Transfers of transfer list
  | Burn_tokens of int64 list
  | Mint_tokens of mint_param list
  | Paused of bool
  | Managed of address list
  | Confirm_admin
  | Confirm_company_wallet

let rec flatten ?(pr=`Pair) = function
  | Mprim { prim; args; annots } when prim = pr ->
    let args = List.fold_left (fun acc x -> match flatten x with
        | Mprim { prim; args; _ } when prim = pr -> acc @ args
        | _ -> acc @ [ x ]) [] args in
    let args = List.map flatten args in
    Mprim { prim; args; annots }
  | Mprim {prim; args; annots} ->
    Mprim {prim; args = List.map flatten args; annots}
  | Mseq l -> Mseq (List.map flatten l)
  | m -> m

let parse_update_operators m =
  let rec aux acc = function
    | [] -> Ok acc
    | Mprim { prim = add; args = [
        Mprim { prim = `Pair; args = [
            Mstring op_owner;
            Mstring op_operator;
            Mint id;
          ]; _ }
      ]; _ } :: t ->
      begin match add with
        | `Left ->
          aux ({op_owner; op_operator; op_token_id = Z.to_int64 id; op_add=true} :: acc) t
        | `Right ->
          aux ({op_owner; op_operator; op_token_id = Z.to_int64 id; op_add=false} :: acc) t
        | _ -> Error `unexpected_michelson_value
      end
    | _ -> Error `unexpected_michelson_value in
  match m with
  | Mseq l -> begin match aux [] l with
      | Error e -> Error e
      | Ok l -> Ok (Operator_updates (List.rev l))
    end
  | _ -> Error `unexpected_michelson_value

let parse_transfer m =
  let rec aux_to acc = function
    | [] -> Ok acc
    | Mprim { prim = `Pair; args = [
        Mstring tr_destination;
        Mint id;
        Mint amount;
      ]; _} :: t ->
      aux_to ({tr_destination; tr_token_id = Z.to_int64 id; tr_amount = Z.to_int64 amount} :: acc) t
    | _ -> Error `unexpected_michelson_value in
  let rec aux_from acc = function
    | [] -> Ok acc
    | Mprim { prim = `Pair; args = [
          Mstring tr_source;
          Mseq txs
        ]; _} :: t ->
      begin match aux_to [] txs with
        | Error e -> Error e
        | Ok txs -> aux_from ({ tr_source; tr_txs = List.rev txs } :: acc) t
      end
    | _ -> Error `unexpected_michelson_value in
  match m with
  | Mseq l -> begin match aux_from [] l with
      | Error e -> Error e
      | Ok l -> Ok (Transfers (List.rev l))
    end
  | _ -> Error `unexpected_michelson_value

let parse_mint m =
  let parse_info l  = List.filter_map (function
      | Mprim { prim = `Elt; args = [ Mstring k; Mbytes h ]; _ } -> Some (k, (h :> string))
      | _ -> None) l in
  match m with
  | Mseq l ->
    Result.ok @@ Mint_tokens (List.concat  (List.map (function
        | Mprim { prim = `Pair; args = [
            Mint meta_token_id; Mseq meta_info; Mseq tokens
          ]; _ } ->
          let mint_meta = parse_info meta_info in
          List.map (function
              | Mprim { prim = `Pair; args = [
                  Mstring mint_owner;
                  Mint token_id;
                ]; _ } ->
                { mint_owner; mint_token_id = Z.to_int64 token_id;
                  mint_meta_token_id = Z.to_int64 meta_token_id;
                  meta_info = mint_meta }
              | _ -> assert false
            ) tokens
        | _ ->
          assert false) l))
  | _ ->
    Error `unexpected_michelson_value

let parse_burn = function
  | Mseq l -> Result.ok @@ Burn_tokens (
      List.filter_map (function Mint id -> Some (Z.to_int64 id) | _ -> None) l)
  | _ -> Error `unexpected_michelson_value

let parse_bool_arg f = function
  | Mprim { prim = `True; args = []; _ } -> Result.ok @@ f true
  | Mprim { prim = `False; args = []; _ } -> Result.ok @@ f false
  | _ ->
    ignore (assert false); Error `unexpected_michelson_value

let parse_paused = parse_bool_arg (fun b -> Paused b)

let parse_managed m =
  match m with
  | Mseq args ->
    Result.ok @@ Managed (
      List.rev_map
        (function
          | Mprim { prim = `Pair; args = [Mstring pk; _]; _ } ->
            begin match Crypto.pk_to_pkh pk with
              | Ok pkh -> pkh
              | Error _ -> assert false
            end;
          | _ ->
            assert false
        )args
    )
  | _ ->
    ignore (assert false); Error `unexpected_michelson_value

let parse_unit_arg f = function
  | Mprim { prim = `Unit ; args = []; _ } -> Result.ok @@ f ()
  | _ -> Error `unexpected_michelson_value

let parse_confirm_admin =
  parse_unit_arg (fun () -> Confirm_admin)

let parse_confirm_company_wallet =
  parse_unit_arg (fun () -> Confirm_company_wallet)

let rec parse_nft e p =
  let p = flatten p in
  match e, p with
  (* main *)
  | EPdefault, Mprim { prim = `Left; args = [ m ]; _ } ->
    parse_nft (EPnamed "left") m

  (* admin *)
  | EPnamed "left", Mprim { prim = `Left; args = [ m ]; _ } ->
    parse_nft (EPnamed "admin") m
  | EPnamed "admin", Mprim { prim = `Left; args = [ m ]; _ } ->
    parse_nft (EPnamed "admin_left") m
  | EPnamed "admin_left", Mprim { prim = `Left; args = [ m ]; _ } ->
    parse_nft (EPnamed "admin_left_left") m
  | EPnamed "admin_left_left", Mprim { prim = `Left; args = [ m ]; _ } ->
    parse_nft (EPnamed "confirm_admin") m
  | EPnamed "admin_left_left", Mprim { prim = `Right; args = [ m ]; _ } ->
    parse_nft (EPnamed "confirm_company_wallet") m
  | EPnamed "admin_left", Mprim { prim = `Right; args = [ m ]; _ } ->
    parse_nft (EPnamed "admin_left_right") m
  | EPnamed "admin_left_right", Mprim { prim = `Left; args = [ m ]; _ } ->
    parse_nft (EPnamed "pause") m
  | EPnamed "admin_left_right", Mprim { prim = `Right; args = [ m ]; _ } ->
    parse_nft (EPnamed "set_admin") m
  | EPnamed "admin", Mprim { prim = `Right; args = [ m ]; _ } ->
    parse_nft (EPnamed "set_company_wallet") m

  (* assets *)
  | EPnamed "left", Mprim { prim = `Right; args = [ m ]; _ } ->
    parse_nft (EPnamed "assets") m
  | EPnamed "assets", Mprim { prim = `Left; args = [ m ]; _ } ->
    parse_nft (EPnamed "fa2") m
  | EPnamed "fa2", Mprim { prim = `Left; args = [ m ]; _ } ->
    parse_nft (EPnamed "fa2_left") m
  | EPnamed "fa2_left", Mprim { prim = `Left; args = [ m ]; _ } ->
    parse_nft (EPnamed "balance_of") m
  | EPnamed "fa2_left", Mprim { prim = `Right; args = [ m ]; _ } ->
    parse_nft (EPnamed "managed") m
  | EPnamed "fa2", Mprim { prim = `Right; args = [ m ]; _ } ->
    parse_nft (EPnamed "fa2_right") m
  | EPnamed "fa2_right", Mprim { prim = `Left; args = [ m ]; _ } ->
    parse_nft (EPnamed "transfer") m
  | EPnamed "fa2_right", Mprim { prim = `Right; args = [ m ]; _ } ->
    parse_nft (EPnamed "update_operators") m
  | EPnamed "assets", Mprim { prim = `Right; args = [ m ]; _ } ->
    parse_nft (EPnamed "token_metadata") m

  (* tokens *)
  | EPdefault, Mprim { prim = `Right; args = [ m ]; _ } ->
    parse_nft (EPnamed "tokens") m
  | EPnamed "tokens", Mprim { prim = `Left; args = [ m ]; _ } ->
    parse_nft (EPnamed "burn_tokens") m
  | EPnamed "tokens", Mprim { prim = `Right; args = [ m ]; _ } ->
    parse_nft (EPnamed "mint_tokens") m

  | EPnamed "update_operators", m -> parse_update_operators m
  | EPnamed "transfer", m -> parse_transfer m
  | EPnamed "mint_tokens", m -> parse_mint m
  | EPnamed "burn_tokens", m -> parse_burn m

  | EPnamed "pause", m -> parse_paused m
  | EPnamed "managed", m -> parse_managed m
  | EPnamed "confirm_admin", m -> parse_confirm_admin m
  | EPnamed "confirm_company_wallet", m -> parse_confirm_company_wallet m

  (* no need to track "set_admin, set_company_wallet, token_metadata and balance_of" ? *)

  | _ -> Error `unexpected_michelson_value
