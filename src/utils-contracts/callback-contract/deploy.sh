pwd
script_dir=`dirname $0`

function deploy (){
    storage=$1
    tezos-client -A granada.tz.functori.com -P 80 originate contract callback-contract-${storage}-balance \
             transferring 0 from wallet-test-admin \
             running file:$script_dir/contract.tez \
             --init $storage --burn-cap 1 --force
}

deploy 0
deploy 1
