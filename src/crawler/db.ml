module PG = Pg.PG
module PGOCaml = PG.PGOCaml
open Hooks
open Proto
open Parameters

type config = {
  admin_wallet: string; [@dft ""]
  company_wallet: string; [@dft ""]
  contract_address: string; [@dft ""]
  mutable max_included_token_id: int64; [@dft -1L]
  mutable max_included_meta_token_id: int64; [@dft -1L]
  mutable max_confirmed_token_id: int64; [@dft -1L]
  mutable max_confirmed_meta_token_id: int64; [@dft -1L]
  paused: bool [@dft true];
  confirmations: int [@dft 0]
} [@@deriving encoding]

let (let>?) p f = Lwt.bind p (function Error e -> Lwt.return_error e | Ok x -> f x)
let (let|>?) p f = Lwt.map (function Error e -> Error e| Ok x -> Ok (f x)) p

let rok = Lwt.return_ok
let rerr = Lwt.return_error

let fold_rp f acc l =
  let rec aux acc = function
    | [] -> Lwt.return_ok acc
    | h :: t ->
      Lwt.bind (f acc h) (function
          | Error e -> Lwt.return_error e
          | Ok acc -> aux acc t) in
  aux acc l

let iter_rp f l = fold_rp (fun () x -> f x) () l

let () = PG.Pool.init ~database:Cconfig.database ()

let use dbh f = match dbh with
  | None -> PG.Pool.use f
  | Some dbh -> f dbh

let one ?(err="expected unique row") l = match l with
  | [ x ] -> rok x
  | _ -> rerr (`hook_error err)

let get_config ?dbh () =
  use dbh @@ fun dbh ->
  let>? l = [%pgsql.object dbh "select * from state"] in
  match l with
  | [] -> rok None
  | [ r ] -> rok @@ Some {
      admin_wallet = r#admin_wallet; company_wallet = r#company_wallet;
      contract_address = r#contract_address;
      max_included_token_id = r#max_included_token_id;
      max_included_meta_token_id = r#max_included_meta_token_id;
      max_confirmed_token_id = r#max_confirmed_token_id;
      max_confirmed_meta_token_id = r#max_confirmed_meta_token_id;
      paused = r#paused; confirmations = r#confirmations }
  | _ :: _ :: _ -> failwith "More than one config are present!"

let set_config ?dbh c =
  use dbh @@ fun dbh ->
  let>? l = [%pgsql.object dbh "select * from state"] in
  match l with
  | [] ->
    [%pgsql dbh
        "insert into state(company_wallet, admin_wallet, contract_address, confirmations, paused) \
         values(${c.company_wallet}, ${c.admin_wallet}, ${c.contract_address}, \
         ${c.confirmations}, ${c.paused})"]
  | _ ->
    [%pgsql dbh
        "update state set company_wallet = ${c.company_wallet}, \
         admin_wallet = ${c.admin_wallet}, \
         contract_address =  ${c.contract_address}, \
         confirmations = ${c.confirmations},
         paused = ${c.paused}"]

let handle_block ~config:_ ?dbh b =
  use dbh @@ fun dbh ->
  [%pgsql dbh "select level from blocks where hash = ${b.Proto.hash}"]

let insert_account dbh addr ~block ~tsp ~level =
  let managed = false in (* default value for managed is False *)
  [%pgsql dbh
      "insert into accounts(address, last_block, last_level, last, managed) \
       values($addr, $block, $level, $tsp, $managed) on conflict do nothing"]

let insert_burns dbh op b =
  let burn = EzEncoding.(construct @@ Json_encoding.list int64) b in
  [%pgsql dbh
      "insert into contract_updates(transaction, index, block, level, tsp, burn) \
       values(${op.bo_hash}, ${op.bo_index}, ${op.bo_block}, ${op.bo_level}, \
       ${op.bo_tsp}, $burn) \
       on conflict do nothing"]

let max_int64 i j = if Int64.compare i j >= 0 then i else j

let insert_mints dbh op l =
  let token_info_enc = Json_encoding.(assoc string) in
  let max_id = ref 0L in
  let max_meta_id = ref 0L in
  let>? () = iter_rp (fun m ->
      max_id := max_int64 !max_id m.mint_token_id;
      max_meta_id := max_int64 !max_meta_id m.mint_meta_token_id;
      let>? () =
          let info = EzEncoding.construct token_info_enc m.meta_info in
          [%pgsql dbh
              "insert into meta_tokens(transaction, block, level, tsp, last_block, \
               last_level, last, meta_token_id, info) \
               values(${op.bo_hash}, ${op.bo_block}, ${op.bo_level}, \
               ${op.bo_tsp}, ${op.bo_block}, ${op.bo_level}, ${op.bo_tsp}, \
               ${m.mint_meta_token_id}, $info) \
               on conflict do nothing"]
      in
      let>? () = [%pgsql dbh
          "insert into tokens(token_id, meta_token_id, block, level, tsp, \
           last_block, last_level, last, owner, transaction) \
           values(${m.mint_token_id}, ${m.mint_meta_token_id}, ${op.bo_block}, ${op.bo_level}, ${op.bo_tsp}, \
           ${op.bo_block}, ${op.bo_level}, ${op.bo_tsp}, '', ${op.bo_hash}) \
           on conflict do nothing"] in
      let>? () =
        if l == [] then rok ()
        else
          [%pgsql dbh
              "update state SET \
               max_included_token_id = \
                 case when ${max_id.contents} > \
               (Select COALESCE(max(max_included_token_id), -1) from state) \
               then ${max_id.contents} \
               else max_included_token_id end, \
               \
               max_included_meta_token_id = \
                 case when ${max_meta_id.contents} > \
               (Select COALESCE(max(max_included_meta_token_id), -1) from state) \
               then ${max_meta_id.contents} \
               else max_included_meta_token_id end"
          ]
      in
      insert_account dbh m.mint_owner ~block:op.bo_block ~level:op.bo_level ~tsp:op.bo_tsp
    ) l in
  let mint = EzEncoding.(construct @@ Json_encoding.list mint_param_enc) l in
  [%pgsql dbh
      "insert into contract_updates(transaction, index, block, level, tsp, mint) \
       values(${op.bo_hash}, ${op.bo_index}, ${op.bo_block}, ${op.bo_level}, \
       ${op.bo_tsp}, $mint) \
       on conflict do nothing"]

let insert_transfers dbh op lt =
  let cpt = ref (-1) in
  let|>? () = iter_rp (fun {tr_source; tr_txs} ->
      let>? () =
        (* probably useless. tr_source is probably already in the accounts table *)
        insert_account dbh tr_source ~block:op.bo_block ~level:op.bo_level ~tsp:op.bo_tsp in
      let|>? () = iter_rp (fun {tr_destination; tr_token_id; _} ->
          incr cpt;
          let pos = Int32.of_int !cpt in
          let>? () =
            [%pgsql dbh
                "insert into token_updates(transaction, index, position, block, level, tsp, \
                 owner, destination, token_id) \
                 values(${op.bo_hash}, ${op.bo_index}, ${pos}, ${op.bo_block}, ${op.bo_level}, \
                 ${op.bo_tsp}, $tr_source, $tr_destination, \
                 $tr_token_id) \
                 on conflict do nothing"] in
          insert_account dbh tr_destination ~block:op.bo_block ~level:op.bo_level ~tsp:op.bo_tsp
        ) tr_txs in
      ()) lt in
  ()

let insert_update_operators dbh op lt =
  let cpt = ref (-1) in
  iter_rp (fun {op_owner; op_operator; op_token_id; op_add} ->
      incr cpt;
      let pos = Int32.of_int !cpt in
      let>? () = [%pgsql dbh
          "insert into token_updates(transaction, index, block, position, level, tsp, \
           owner, operator, add, token_id) \
           values(${op.bo_hash}, ${op.bo_index}, ${op.bo_block}, ${pos}, ${op.bo_level}, \
           ${op.bo_tsp}, $op_owner, $op_operator, $op_add, \
           $op_token_id) on conflict do nothing"] in
      let>? () = [%pgsql dbh
          "insert into operators(token_id, owner, operators, \
           last_block, last_level, last) VALUES \
           (${op_token_id}, $op_owner, '{}', ${op.bo_block}, ${op.bo_level}, \
           ${op.bo_tsp}) on conflict do nothing"] in
      insert_account dbh op_operator ~block:op.bo_block ~level:op.bo_level ~tsp:op.bo_tsp) lt

[@@@ocaml.warning "-33"]
let string_of_wallets_update_enum s = s
let wallets_update_enum_of_string s = s

let insert_to_wallets_updates dbh op address kind ~pos =
  [%pgsql dbh
      "insert into wallets_updates(transaction, index, position, block, level, tsp, \
       address, update_kind) \
       values(${op.bo_hash}, ${op.bo_index}, ${pos}, ${op.bo_block}, ${op.bo_level}, \
       ${op.bo_tsp}, $address, $kind) on conflict do nothing"]

let insert_managed dbh op l =
  let kind = "Manage" in
  let cpt = ref (-1) in
  iter_rp (fun managed ->
      incr cpt;
      let>? () = insert_to_wallets_updates dbh op managed kind ~pos:(Int32.of_int !cpt) in
      insert_account dbh managed ~block:op.bo_block ~level:op.bo_level ~tsp:op.bo_tsp
    ) l

let insert_paused dbh op paused_flag =
  let kind = if paused_flag then "Pause" else "Unpause" in
  insert_to_wallets_updates dbh op op.bo_op.source kind ~pos:0l

let insert_confirm_company_wallet dbh op =
  insert_to_wallets_updates dbh op op.bo_op.source "Confirm_company_wallet" ~pos:0l

let insert_confirm_admin dbh op =
  insert_to_wallets_updates dbh op op.bo_op.source "Confirm_admin" ~pos:0l

let insert_transaction dbh op tr =
  match tr.parameters with
  | Some {entrypoint; value = Micheline m} ->
    begin match parse_nft entrypoint m with
      | Ok (Mint_tokens m) -> insert_mints dbh op m
      | Ok (Burn_tokens b) -> insert_burns dbh op b
      | Ok (Transfers t) -> insert_transfers dbh op t
      | Ok (Operator_updates t) -> insert_update_operators dbh op t
      | Ok (Paused bflag) -> insert_paused dbh op bflag
      | Ok (Managed l) -> insert_managed dbh op l
      | Ok Confirm_admin -> insert_confirm_admin dbh op
      | Ok Confirm_company_wallet -> insert_confirm_company_wallet dbh op
      | _ -> Lwt.return_ok ()
    end
  | _ -> Lwt.return_ok ()

let handle_operation ~config:_ ~dbh op =
  match op.bo_op.kind with
  | Transaction tr -> insert_transaction dbh op tr
  | _ -> Lwt.return_ok ()

let burn_updates dbh ~main ~block ~level ~tsp l =
  iter_rp (fun token_id ->
      let>? l = [%pgsql dbh
          "update tokens set \
           burnt = $main, \
           last_block = case when $main then $block else last_block end, \
           last_level = case when $main then $level else last_level end, \
           last = case when $main then $tsp else last end \
           where token_id = $token_id returning meta_token_id, owner"] in
      let>? meta_token_id, owner = one l in
      let value = if main then 1L else -1L in
      let>? () = [%pgsql dbh
          "update meta_tokens set \
           current_supply = current_supply - $value, \
           last_block = case when $main then $block else last_block end, \
           last_level = case when $main then $level else last_level end, \
           last = case when $main then $tsp else last end \
           where meta_token_id = $meta_token_id"] in
      [%pgsql dbh
        "update accounts set tokens = \
         case when $main then array_remove(tokens, $token_id) \
         else array_append(tokens, $token_id) end, \
         last_block = case when $main then $block else last_block end, \
         last_level = case when $main then $level else last_level end, \
         last = case when $main then $tsp else last end \
         where address = $owner"]) l

let mint_updates dbh ~main ~block ~level ~tsp l
    max_confirmed_id max_confirmed_meta_id =
  iter_rp (fun {mint_token_id; mint_owner; mint_meta_token_id; _} ->
      max_confirmed_id :=  max_int64 !max_confirmed_id mint_token_id;
      max_confirmed_meta_id :=  max_int64 !max_confirmed_meta_id mint_meta_token_id;
      let owner = if main then mint_owner else "" in
      let>? () = [%pgsql dbh
          "update tokens set owner = $owner where token_id = $mint_token_id"] in
      let value = if main then 1L else -1L in
      let>? () = [%pgsql dbh
          "update meta_tokens set \
           current_supply = current_supply + $value, \
           last_block = case when $main then $block else last_block end, \
           last_level = case when $main then $level else last_level end, \
           last = case when $main then $tsp else last end \
           where meta_token_id = $mint_meta_token_id"] in
      [%pgsql dbh
          "update accounts set tokens = \
           case when $main then array_append(tokens, $mint_token_id) \
           else array_remove(tokens, $mint_token_id) end, \
           last_block = case when $main then $block else last_block end, \
           last_level = case when $main then $level else last_level end, \
           last = case when $main then $tsp else last end \
           where address = $mint_owner"]) l

let contract_updates dbh main l =
  let max_confirmed_id = ref (-1L) in
  let max_confirmed_meta_id = ref (-1L) in
  let>? () = iter_rp (fun r ->
      let block, level, tsp = r#block, r#level, r#tsp in
      let>? n = match r#mint, r#burn with
        | Some json, _ ->
          let l = EzEncoding.destruct (Json_encoding.list mint_param_enc) json in
          let>? () =
            mint_updates dbh ~main ~block ~level ~tsp l
              max_confirmed_id max_confirmed_meta_id in
          rok (Int64.of_int @@ List.length l)
        | _, Some json ->
          let l = EzEncoding.(destruct @@ Json_encoding.list int64) json in
          let>? () = burn_updates dbh ~main ~block ~level ~tsp l in
          rok (Int64.neg (Int64.of_int @@ List.length l))
        | _ -> Lwt.return_ok 0L in
      let n = if main then n else Int64.mul (-1L) n in
      [%pgsql dbh
          "update state set \
           number_of_tokens = number_of_tokens + $n, \
           last = case when $main then $tsp else last end"]) l in
  if l == [] then rok ()
  else if main then
    [%pgsql dbh
        "update state SET \
         max_confirmed_token_id = \
         case when ${max_confirmed_id.contents} > \
         (Select COALESCE(max(max_confirmed_token_id), -1) from state) \
         then ${max_confirmed_id.contents} \
         else max_confirmed_token_id end, \
         \
         max_confirmed_meta_token_id = \
         case when ${max_confirmed_meta_id.contents} > \
         (Select COALESCE(max(max_confirmed_meta_token_id), -1) from state) \
         then ${max_confirmed_meta_id.contents} \
         else max_confirmed_meta_token_id end"
    ]
  else
    [%pgsql dbh
        "update state SET \
         max_confirmed_token_id = \
         (Select COALESCE(max(token_id), -1) from tokens where confirmed),
         \
         max_confirmed_meta_token_id = \
         (Select COALESCE(max(meta_token_id), -1) from meta_tokens where confirmed)"
    ]

let operator_updates dbh ~token_id ~operator ~add ~block ~level ~tsp ~owner main =
  [%pgsql dbh
      "update operators set \
       operators = case when ($main and $add) or (not $main and not $add) then \
       array_append(operators, $operator) else array_remove(operators, $operator) end, \
       last_block = case when $main then $block else last_block end, \
       last_level = case when $main then $level else last_level end, \
       last = case when $main then $tsp else last end \
       where owner = $owner and token_id = $token_id"]

let transfer_updates dbh main ~block ~level ~tsp ~token_id ~owner destination =
  let>? () = [%pgsql dbh
      "update tokens set \
       owner = case when $main then $destination else $owner end, \
       last_block = case when $main then $block else last_block end, \
       last_level = case when $main then $level else last_level end, \
       last = case when $main then $tsp else last end \
       where token_id = $token_id"] in
  let>? () = [%pgsql dbh
      "update accounts set
       tokens = case when $main then array_append(tokens, $token_id) \
       else array_remove(tokens, $token_id) end, \
       last_block = case when $main then $block else last_block end, \
       last_level = case when $main then $level else last_level end, \
       last = case when $main then $tsp else last end \
       where address = $destination"] in
  [%pgsql dbh
      "update accounts set
       tokens = case when $main then array_remove(tokens, $token_id) \
       else array_append(tokens, $token_id) end, \
       last_block = case when $main then $block else last_block end, \
       last_level = case when $main then $level else last_level end, \
       last = case when $main then $tsp else last end \
       where address = $owner"]

let token_updates dbh main l =
  iter_rp (fun r ->
      let block, level, tsp, owner, token_id =
        r#block, r#level, r#tsp, r#owner, r#token_id in
      match r#destination, r#operator, r#add  with
      | Some destination, _, _ ->
        transfer_updates dbh main ~block ~level ~tsp ~token_id ~owner destination
      | _, Some operator, Some add ->
        operator_updates dbh ~operator ~add ~block ~level ~tsp ~token_id ~owner main
      | _ -> Lwt.return_error (`hook_error "invalid token_update")) l


let update_managed dbh m_main managed ~managed_flag =
  let>? new_flag =
    if m_main then
      (* simple case, confirme last op *)
      rok managed_flag
    else
      (* complicated case: retrieve old value *)
      let>? l =
        [%pgsql dbh
            "select update_kind from wallets_updates \
             where address = $managed AND main = true AND \
             (update_kind = 'Manage') \
             order by level desc, index desc limit 1"
        ]in
      rok @@
      match l with
      | [] -> false (* default value of managed is false*)
      | ["Manage"] -> true
      | _ -> failwith ("Not reachable: " ^ __LOC__)
  in
  [%pgsql dbh
      "update accounts set managed = $new_flag where address = $managed"]

let update_paused dbh m_main ~paused_flag =
  let>? new_flag =
    if m_main then
      (* simple case, confirme last op *)
      rok paused_flag
    else
      (* complicated case: retrieve old value *)
      let>? l =
        [%pgsql dbh
            "select update_kind from wallets_updates \
             where main = true AND (update_kind = 'Pause' OR update_kind = 'Unpause') \
             order by level desc, index desc limit 1"
        ] in
      rok @@
      match l with
      | [] -> true (* TODO: Current deploy is with pause = true *)
      | ["Pause"] -> true
      | ["Unpause"] -> false
      | _ -> failwith ("Not reachable: " ^ __LOC__)
  in
  [%pgsql dbh
      "update state set paused = $new_flag"]

let update_confirm_admin dbh m_main address =
  let>? new_admin =
    if m_main then
      (* simple case, confirme last op *)
      rok address
    else
      (* complicated case: retrieve old value *)
      let>? l =
        [%pgsql dbh
            "select address from wallets_updates \
             where main = true AND update_kind = 'Confirm_admin' \
             order by level desc, index desc limit 1"
        ] in
      rok @@
      match l with
      | [] -> assert false (* TODO: Get it from deploy infos *)
      | [old_admin] -> old_admin
      | _ -> failwith ("Not reachable: " ^ __LOC__)
  in
  [%pgsql dbh "update state set admin_wallet = $new_admin"]


let update_confirm_company_wallet dbh m_main address =
  let>? new_cwallet =
    if m_main then
      (* simple case, confirme last op *)
      rok address
    else
      (* complicated case: retrieve old value *)
      let>? l =
        [%pgsql dbh
            "select address from wallets_updates \
             where main = true AND update_kind = 'Confirm_company_wallet' \
             order by level desc, index desc limit 1"
        ] in
      rok @@
      match l with
      | [] -> assert false (* TODO: Get it from deploy infos *)
      | [old_cwallet] -> old_cwallet
      | _ -> failwith ("Not reachable: " ^ __LOC__)
  in
  [%pgsql dbh "update state set company_wallet = $new_cwallet"]


let wallets_updates dbh m_main l =
  iter_rp (fun r ->
      let address = r#address in
      match r#update_kind with
      | "Confirm_admin" -> update_confirm_admin dbh m_main address
      | "Confirm_company_wallet" -> update_confirm_company_wallet dbh m_main address
      | "Pause" -> update_paused dbh m_main ~paused_flag:true
      | "Unpause" -> update_paused dbh m_main ~paused_flag:false
      | "Manage" -> update_managed dbh m_main address ~managed_flag:true
      | _ -> assert false
    ) l

let handle_main =
  (* only for cases where main becomes true (ie for confirmations) *)
  let sort_by_insert_order l =
    List.fast_sort (fun a b -> Int32.compare a#index b#index) l
  in
  fun ~config:_ ~dbh {m_main; m_hash} ->
    let>? () = [%pgsql dbh "update token_updates set main = $m_main where block = $m_hash"] in
    let>? () = [%pgsql dbh "update contract_updates set main = $m_main where block = $m_hash"] in
    let>? () = [%pgsql dbh "update wallets_updates set main = $m_main where block = $m_hash "] in
    if m_main then
      let>? confirmed_hash = [%pgsql dbh "select blocks.hash from blocks, state \
                                          where main AND \
                                          level = (select level from blocks where hash = ${m_hash} limit 1) - \
                                          state.confirmations "] in
      match confirmed_hash with
      | [] -> rok () (* Nothing to confirm yet *)
      | _::_::_ -> assert false
      | [confirmed_hash] ->
        let>? () = [%pgsql dbh "update tokens set confirmed = true where block = $confirmed_hash"] in
        let>? () = [%pgsql dbh "update meta_tokens set confirmed = true where block = $confirmed_hash"] in

        let>? t_updates = [%pgsql.object dbh "update token_updates set confirmed = true where block = $confirmed_hash returning *"] in
        let>? c_updates = [%pgsql.object dbh "update contract_updates set confirmed = true where block = $confirmed_hash returning *"] in
        let>? w_updates = [%pgsql.object dbh "update wallets_updates set confirmed = true where block = $confirmed_hash returning *"] in
        let>? () = contract_updates dbh true @@ sort_by_insert_order c_updates in
        let>? () = token_updates dbh true @@ sort_by_insert_order t_updates in
        wallets_updates dbh true @@ sort_by_insert_order w_updates
    else begin
      Format.eprintf
        "Warning: chain backtrack detected. If some confirmed operations are backtracked onchain, they'll not be reverted@.";
      rok ()
    end
