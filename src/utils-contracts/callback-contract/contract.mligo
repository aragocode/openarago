(*
This contract is intended to serve as a callback to test entrypoint "balance_of" of FA2 NFT contract.
Two versions should be deployed:
- one, with storage = 1p, to serve as a callback for balances equal to 1
- one with storage = 0p, as a callback for balances equal to 0
*)
type storage = {
    expected_NFT_balance : nat ; (* 0 or 1 at deploy time *)
}

type token_id = nat

type balance_of_request = [@layout:comb] {
  owner : address;
  token_id : token_id;
}

type balance_of_response = [@layout:comb] {
  request : balance_of_request;
  balance : nat;
}

type parameter = balance_of_response list

type return = operation list * storage

let main (p, store : parameter * storage) : return =
    let () = List.iter (fun (b : balance_of_response) ->
        if b.balance = store.expected_NFT_balance then ()
        else (failwith "CALLBACK:UNEXPECTED_BALANCE_RESULT" : unit)
    ) p in
    ([] : operation list), store
