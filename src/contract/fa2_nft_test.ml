open Mligo
(* open List *)
include Fa2_nft_asset

let src = Test.nth_bootstrap_account 0
let other = Test.nth_bootstrap_account 1

let initial_storage : storage = {
  admin = src;
  company_wallet = src;
  pending_admin = (None : address option);
  pending_company_wallet = (None : address option);
  paused = true;
  ledger = (Big_map.empty : (token_id, address) big_map);
  operators = (Big_map.empty : operator_storage);
  managed = (Big_map.empty : managed_storage);
  next_meta_token_id = 0;
  _NB_TOKENS_PER_META = 8n;
  _META_TOKEN_SHIFT =  1000;
  meta_token_info = (Big_map.empty : (meta_token_id, meta_token_info) big_map);
  meta_token_supply = (Big_map.empty : (meta_token_id, token_amount) big_map);
  untransferable = (Big_map.empty : ((token_id * address), hashed_secret) big_map);
  metadata = (Big_map.empty : (string, bytes) big_map)
}

let test_mint =
  let () = Test.set_source src in
  let (taddr, _, _) = Test.originate nft_asset_main initial_storage 0t in
  let contr = Test.to_contract taddr in
  let storage = Test.get_storage taddr in
  let old_meta_token_cpt = storage.next_meta_token_id in
  let mint_param1 =
    { meta_token_id = 1;
      token_info = (Map.empty : (string, bytes) map);
      tokens = [{token_id = 1001; owner = src};
                {token_id = 1002; owner = src}]
    }
  in
  let mint_param2 =
    { meta_token_id = 2;
      token_info = (Map.empty : (string, bytes) map);
      tokens = [{token_id = 2001; owner = src};
                {token_id = 2002; owner = src}]
    }
  in
  let mint_param = [ mint_param1 ; mint_param2 ] in
  let () =
    Test.transfer_to_contract_exn contr (Tokens (Mint_tokens mint_param) ) 0t in
  let storage = Test.get_storage taddr in
  let () = assert (old_meta_token_cpt +2 = storage.next_meta_token_id) in
  ()
