open Crawlori
open Rp
open Config
module Crawler = Make(Pg)(struct type extra = Db.config end)

let verbose =
  try Sys.getenv "FUNCTORI_NFT_CRAWLER_VERBOSE" = "true"
  with _ -> false


let block_hook config dbh (b : Proto.full_block) =
  if verbose then
    Format.printf "Register block:\n%s\n@." @@
    EzEncoding.construct Proto.full_block_enc.json b;
  let@ _ = Db.handle_block ~config ~dbh b in
  rok ()

let operation_hook config dbh (op : Hooks.block_op) =
  if verbose then
    Format.printf "Register operation:\n%s\n@." @@
    EzEncoding.construct Proto.manager_operation_info_enc.json op.bo_op;
  let@ _ = Db.handle_operation ~config ~dbh op in
  rok ()

let hook config dbh (m : Hooks.main_arg) =
  if verbose then
    Format.printf "Set main: %s, %B\n@." m.m_hash m.m_main;
  let@ _ = Db.handle_main ~config ~dbh m in
  rok ()

let fill_config c_json =
  let@ c_db = Db.get_config () in
  let extra = c_json.extra in
  let extra = match c_db with
    | None -> extra
    | Some c_db ->
      let c_db =
        if extra.Db.contract_address <> "" then {c_db with contract_address = extra.Db.contract_address}
        else c_db in
      let c_db =
        if extra.Db.admin_wallet <> "" then {c_db with admin_wallet = extra.Db.admin_wallet}
        else c_db in
      let c_db =
        if extra.Db.company_wallet <> "" then {c_db with company_wallet = extra.Db.company_wallet}
        else c_db in
      c_db in
  if extra.contract_address = "" then rerr (`hook_error "No contract address given")
  else
    let@ () = Db.set_config extra in
    let () = match c_json.accounts with
      | None ->
        c_json.accounts <- Some (SSet.singleton extra.Db.contract_address)
      | Some s ->
        c_json.accounts <- Some (SSet.add extra.Db.contract_address s) in
    rok { c_json with extra }

let () =
  Lwt_main.run @@
  Lwt.map (fun _ -> ()) @@
  let@ config = Unix_sys.get_config Db.config_enc in
  let@ config = fill_config config in
  Crawler.Hooks.set_block block_hook;
  Crawler.Hooks.set_operation operation_hook;
  Crawler.Hooks.set_main hook;
  let@ () = Crawler.init config in
  Crawler.loop config
