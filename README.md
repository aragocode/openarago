# NFT

NFT smart contract along with:
- deploy scripts
- typescript interface, to build injectors
- entrypoints tests
- simple crawler


## 1. Repository Structure

This repository is organized as follows:

- `src/contract`: contains smart contracts files in `.ml` format and a `dune` to generate `.mligo` files from them
- `src/ts:` contains two Typescript files (libraries)
  - `crypto.ts`: provides facilities to generate fresh tezos keys
  - `fa2_nft.ts` provides facilities to interface with the smart contracts on the blockchain
- `src/crawler`: is a template crawler to be parameterized / extended for the project
- `src/script`: is a script file (in OCaml) to compile and deploy the smart contracts, and call different entrypoints. It is used via the toplevel .mlt file `contract-script.mlt`
- `vendors`: contains a set of external deps (currently the `crawlori` library used by the crawler template)

## 2. Requirements / Setting up Dev Environments

### 2.1. Installing a Postgresql database server

You'll first need to install a [Postgresql database server](https://www.postgresql.org). The whole process has been tested with version 13.4. Once Postgresql is correctly installed and configured, create a user as follows:

```bash
sudo -i -u postgres
psql
CREATE USER <user>;
ALTER ROLE <user> CREATEDB;
CTRL+D (to quit)
```

Here `<user>` is intended to be the UNIX user running the infra.

### 2.2. Customizing database name

The default database that will be created/used by the crawler (template) is `DB=functori_nft_db`. To customize this name,  create a `Makefile.config` file with something like:
```shell
DB=my_db_name
```

Note that symbols like spaces or `-` are forbidden in the database name.

### 2.3. Npm dev env

Install `node` and `npm` on your machine (the process has been successfully tested with `npm` version `6.14.14` and `node` version `16.6.2`). Then, run:

```bash
$ make ts-deps
```

The command above will:
- install the `typescript` compiler (sudo privileges are needed for it)
- install the `taquito` library (needed by the interface code to the smart contract), and the `hacl-wasm` library (used by the keys generation lib).

Also, run the following command to install some additional libraries, used in particular when testing.
```bash
$ make node-deps
```

### 2.4. OCaml dev env

A working OCaml environment is needed if you want to compile/deploy the smart contract, or compile the crawler.

Similarly to npm for nodeJS or Cargo for Haskell, OCaml has its own packages manager called `opam`. When installing OCaml dependencies, OPAM should be able to detect system dependencies that you should install as well.

- start by installing the latest version of OPAM by following the guide here: https://opam.ocaml.org/doc/Install.html

- then, run the following command to initialize opam
```bash
$ opam init --reinit --bare --disable-sandboxing
```
- once this is done, the following command should create an `_opam` directory at the root of this repository and install the OCaml compiler version 4.12.0:
```bash
$ make opam-switch
```

- then, running the following target should install needed dependencies Note that you may be asked to install system dependencies during the process:
```bash
$ make build-deps
```

### 2.5. Tezos Client: Getting/Compiling Tezos

If you want to deploy the smart contract and/or interact with its entrypoints using the Tezos command-line client (called `tezos-client`), you need:
- to compile tezos from sources. For that,
follow the instructions on gitlab [Tezos documentation](https://tezos.gitlab.io/introduction/howtoget.html) and be sure `tezos-client` is installed, or exported in the `$PATH`;

- or to use a docker image;

- or, to install the Tezos version released as an OPAM package.

To play with the contract (deployment, calling entrypoints), you may want to use a Tezos testnet (the current one is called granadanet). For that, you should first get faucet account and some free tokens on the [Faucet](https://faucet.tzalpha.net/) website.

If you downloaded a JSON faucet from the website above, we can activate it with a command of the form:

``` bash
  $ tezos-client -A granada.tz.functori.com -P 80 activate account my-test-wallet with <tz-file>.json
```

We will use the `my-test-wallet` below as the admin who deploys and administrates the NFT smart contract.

### 2.6. Ligo Smart Contracts Language

To be able to compile the smart contract, you need to install the LIGO compiler. The `make build-deps` above is already supposed to install it. If not, follow the instructions on the [Ligo](https://ligolang.org/docs/intro/installation) official website and be sure `ligo` compiler is installed. You can also use the provided docker image.



## 3. Building/Deploying

### 3.1. Building javascript files from typescript

To generate javascript files from the typescript, just run:

```bash
  $ make ts
```

The generated javascript files `fa2_nft.js` and `crypto.js` will be located in `src/ts/dist`. Some examples on how to use these libraries are given in the next sections.

### 3.2. Building the NFT smart contract

If the OCaml dev environment is correctly installed, the following command should allow you to build the smart contract (ie. generate Camligo files from Ml files, and then Michelson files from Camligo):

```bash
  $ make build-contracts
```

Generated files are stored in `_build/default/src/contract/`. In particular, make sure you have a file named `fa2_nft_asset.tz` (the full contract in Michelson) and a file named `fa2_nft_storage.tz` (the initial storage used when deploying the contract).

### 3.3. Deploying the NFT smart contract

Now, let's see how to deploy the smart contract we just compiled.
We assume `tezos-client` is installed or available in the `$PATH` environment (otherwise an option `--client PATH_TO_TEZOS_CLIENT` is needed to indicate its path).

Let's start by importing a wallet (we called it `wallet-test-admin`) that will use as the contract's admin. This wallet is already hardcoded in the tests of the smart contracts and the Typescript interface library in `src/ts/tests/entrypoints-tests.js`. Its public address is `tz1XepEWA8GFp4Np4kqpHBhxiNHYiCnmE9bt`. To import the private key, in order to deploy the contract, just run:

```bash
  $ tezos-client import secret key wallet-test-admin unencrypted:edsk3bdvhP1b1gJgR7uT9t22c8ECFL5LWDmNUzmZqYo2NqkDYx6GKA --force
```

If the wallet has been added, you should now be able to deploy the smart contract with:

```bash
./contract-script.mlt deploy --source tz1XepEWA8GFp4Np4kqpHBhxiNHYiCnmE9bt --endpoint http://granada.tz.functori.com --contract fa2_nft_asset__<N>
```

Where `<N>` is the N-th contract you're about to originate (just to give different names for successively originated contracts).

Depending on your situation, you may need to pass some extra arguments to the script. The command `./contract-script.mlt --help` should display the list of available options.

## 4. Getting familiar with Tezos keys and accounts

There are in Tezos two kind of accounts:
- Implicit addresses, prefixed with `tz`. These are regular wallets. Each of them is composed of the public key hash (the public address `tz`), the corresponding private key, used to sign operations, and the public key used to check signed messages. As its name suggests, the address is `just` the hash of the public key, appended to some prefix.

- Originated addresses, prefixed with `KT`. These are the Tezos smart contracts. They don't have their own public or secret keys. The interactions that smart contracts could make or accept are defined by their own codes.


### 4.1. Generating keys with tezos-client

Fresh wallets generation with tezos-client can be done as follows:

```bash
  $ tezos-client gen keys my-new-wallet --force
```

The `--force` option allows to be overwrite the alias name if it's already bound to some keys (to be used with caution on mainnet to avoid loosing funds). Generated keys are (usually) stored in three JSON files (for public keys, private keys and public key hashs) in the `$HOME/.tezos-client`. Accessing the keys for a wallet can be done as follows (adding option `-S` will also display private/secret key):
```bash
  $ tezos client show address my-new-wallet
```

The wallet you just created has no tez. You can transfer some amount to it from `wallet-test-admin` or from the activated wallet `my-test-wallet` as follows:

```bash
  $ tezos-client -A granada.tz.functori.com -P 80 transfer 2 from wallet-test-admin to my-new-wallet --burn-cap 0.3
```

To check the balance of an account `<wallet-alias>` whose tz1 address is `<tz1ZZZ>`, you can either execute the following `tezos-client` command:

```bash
  $ tezos-client -A granada.tz.functori.com -P 80 get balance for  <wallet-alias>
```

run the following `curl` command (amounts are in sub-units. 1Tez = 10^6 muTez):

```bash
  $ curl http://granada.tz.functori.com:80/chains/main/blocks/head/context/contracts/tz1gL1ZHgsjKGJyJ44GGZ78Kbmd8XugjPjzc/balance
```

Or check on an online block explorer supporting the granadanet testnet, like: https://granadanet.tzkt.io/tz1ZZZ. Make sure to replace the placeholder `tz1ZZZ` by the tz1 address, and remove the `granadanet.` prefix if you target the mainnet.


### 4.2. Generating keys with the javascript library

Having a file `main.js` (assumed to be in `src/ts/dist` for this example) with the following content:

```js
const crypto = require ("./crypto.js");

async function foo(){
    let x = await crypto.generate_keys();
    console.log ("generated keys are: " + JSON.stringify (x)) ;
}

foo ()
```

and running the following command

```bash
 $ node main.js
```

Should allow to generate keys of the form:
```json
{
  "sk":"edsk3xmVq4rsFPk5M3kxeoq2ijBK6sZ2MabczCAjJBgjwRW63btB1e",
  "pk":"edpku7uAXYrafBPi1CVAHj8FAfQMhBVJqsqQfTGn6Yi86WJuVHmM2D",
  "pkh":"tz1RYiCDNN71rixLS3FkhowYuvb5QwvE1Egp"
}
```
where `sk` is the secret key, `pk` its corresponding public key, and `pkh` the hash of the public key (that plays the role of account/wallet address on Tezos blockchain as said previously).

Note that a private key could be imported by/to tezos-client with the following command:
```bash
  $ tezos-client import secret key <my-alias> unencrypted:edsk3xmVq4rsFPk5M3kxeoq2ijBK6sZ2MabczCAjJBgjwRW63btB1e
```

Importing the private key remains to import the three keys, as public keys and public key hashs could be derived from the private key. Note that keys generated by the library are `unencrypted` (hence the prefix for `tezos-client`, which also support encrypted secret keys).

## 5. Interacting with the Smart Contracts

### 5.1. Library: with nodeJS interfaces to entrypoints

In the provided Typscript library, every entrypoint of the smart contract is wrapped behind am `async` function, to ease interacting with it programmatically. Let's see code snippet showing how to invoke the **pause/unpause** entrypoint:

Having a file `main.js` (assumed to be in `src/ts/dist` for this example) with the following content:

```js
const nft = require ("./fa2_nft.js");

const { TezosToolkit } = require ('@taquito/taquito');
const { InMemorySigner, importKey } = require ('@taquito/signer');

const tezosKit = new TezosToolkit('http://granada.tz.functori.com:80');
const ADMIN_PRIVATE_KEY__BAD = 'edsk36AvDtuTQGwV4QA5mAAP3mJb5NNxFfSh9LGwSGMoBcjPMqfNmC';

// EDIT HERE TO PUT THE CORRECT PRIVATE KEY OF THE CONTRACT's ADMIN!!!
const ADMIN_PRIVATE_KEY__GOOD = 'edsk2ovP8p2gpSurQ7xBBSUsC91uFz9UzqVr8Pjurozb2T1ykbpL9p';
const ADMIN_PRIVATE_KEY = ADMIN_PRIVATE_KEY__GOOD;

tezosKit.setProvider({
  signer: new InMemorySigner(ADMIN_PRIVATE_KEY),
});

async function foo(){
    try{
        // EDIT HERE TO PUT THE CORRECT KT1 ADDRESS!!!
        let kt1_addr = "KT1VZ4P6LJGFnqttCKjEskMDKNrrnbRfbGzp";
        console.log ("Calling an entrypoint");
        let x = await nft.unpause (tezosKit, kt1_addr);
        console.log ("Operation injected with hash: " + JSON.stringify (x)) ;
    }catch (e){
        console.log ('An error occured while injecting the operation.\n');
        console.log (JSON.stringify(e));
        console.log ('\nMake sure the caller is authorised on this entrypoint, the wallet is funded, the KT1 address is correct, ....');
    }
}

foo ()
```

and running the following command
```bash
node main.js
```
should print something similar to the following:

```bash
Calling an entrypoint
Operation injected with hash: "ooD2xdMop6ir9y2AqNXUcuHquYBLDmfYUczQjhyzu7SWfrYM4ES"
```

If so, you've successfully called the entrypoint `unpause` of the smart contract whose KT1 address is encoded in the function `foo`.  Note that if the contract is already unpaused, calling `unpause` doesn't fail, but the call is useless (this is how pause/unpause is implemented).

Of course, this only works if/when the wallet used to make the call is the admin's. if you modify the line
defining the constant `ADMIN_PRIVATE_KEY` as follows:

```js
    const ADMIN_PRIVATE_KEY = ADMIN_PRIVATE_KEY__BAD;
```

and run `node main.js` again, you should obtain an error, because this key is not an admin's. So it is not allowed to pause/unpause the smart contract.

You can find examples on how to call the other entrypoints in the following tests file in this Git repository: `src/ts/tests/entrypoints-tests.js`

### 5.2. Library: batched mode

In Tezos, it is in general hard to make more than one transaction at once from a `tz` account. Indeed, every transaction from a `tz` account is associated to a counter, and it may be hard to correctly deal with the counter to inject and propagate transactions to the rest of the network in the correct order.

An alternative/solution for this is to batch transactions. It consists in bundling several small transactions in a bug one. Of course, there is a limit: A transaction's weight (in bytes and in consumed gas) has an upper bound in Tezos.

In the provided Typescript library, there is an async function called `batch` that takes a list of transactions to batch and inject then in the blockchain. Again, a working example can be found in `src/ts/tests/entrypoints-tests.js` (tests in function `batch`).


### 5.3. With the provided OCaml Script

The provided OCaml script that helps compiling and deploying the smart contract can also be used to call its entrypoints. If you want to use it, you may want to ask for help with:
```bash
./contract-script.mlt --help
```

Of course, this script is intended for testing. To programmatically call the entrypoints from another code, we advise to use the typescript/ nodeJs library.


## 6. Testing the Smart Contract's entrypoints

``` sh
TODO: update this section -> fa2_nft_simple_workflow.js is simpler
```

The NodeJS file `src/ts/tests/entrypoints-tests.js` allows to test both the Typescript interface library and all the entrypoints of the smart contract. For each entrypoint, a list of tests that target different cases are provided. A more detailed list of what have been exactly tested will be provided in the final report of the project.

To replay the tests, go into `src/ts/tests/entrypoints-tests.js` and run:
```bash
  $ node fa2_nft_simple_workflow.js
  $ node entrypoints-tests.js
```

## 7. Crawling the Smart Contract's calls


``` sh
TODO: update this section to recent changes in the DB
```


A standalone crawler is provided in the this Git repository (in src/crawler). It is built on top of Functori's crawling library `Crawlori`. If not already done, run the following command to build the crawler's binary:
```
  $ make build
```

If the command above fails, try it a second time (the command also create the crawler's database, and when called for the first time, it may fail to run things in the right order). If the command still fails, this likely means that you have probably miss-configured or not started the PostgreSQL server, or that you have issues with the required dependencies.

Once the build succeeds, you should be able to:
- connect to the database of the project (eg. with `psql <database_name>`), and see the tables created there (eg. with `\d` in `psql` session)
- launch the crawler binary located in `_build/default/src/crawler/crawler.exe`

To start the crawler, just run the following command:
```
  $ _build/default/src/crawler/crawler.exe <config.json>
```

where `<config.json>` is a config file that provides different parameters for the crawler. A template of such a file is given in `crawler-config-template.json`. To provide a correct configuration file, make sure to:
- use a correct node address (the template is configured to use a Functori's testnet node). For in-production setting, it is advised to use your own mainnet nodes.
- start crawling at the appropriate blockchain level (for instance, we increase the level in the config template regularly when testing, to avoid waiting hours before our newly generated contracts are crawled)
- provide the address of the contract you want to crawl. The config template contains one of the contract we used during our tests (testing the smart contract, the TS library, and the crawler).

Additional fields that can be provided in the config file are `ledger_id` (the int ID of the ledger bigmap in the smart contract), the `admin_wallet` and the `company_wallet`.

The database's table can be split into three categories:

- Tables needed, populated by the `Crawlori` library. These table (listed below) are needed by the library to work correctly, we will just ignore their content.
  ```
  public | blocks           | table
  public | delegations      | table
  public | originations     | table
  public | transactions     | table
  public | predecessors     | table
  public | reveals          | table
  public | balance_updates  | table
  public | ezpg_downgrades  | table
  public | ezpg_info        | table
  public | ezpg_upgrades    | table
  ```

- Tables created by the crawler to track the smart contract's calls in a chronological order, to easily detect when operations are confirmed or reverted. These are a transcription of the smart contract's call in the database:

  ```
   public | contract_updates | table
   public | token_updates    | table
   public | wallets_updates  | table
  ```

- Tables created by the crawler to reflect the state of the smart contract's storage at any time (modulo the latest included operations that are not confirmed yet).
  ```
   public | tokens           | table
   public | meta_tokens      | table
   public | accounts         | table
   public | operators        | table
   public | state            | table
  ```
  - the table `tokens` lists the (minted) tokens with their associated information, like the owner, the associated meta_token_id, the burnt flag, etc.
  - the table `meta_tokens` provides information about meta tokens like the current/max supply, their metadata, etc.
  - the table `accounts` stores the addresses that have been encountered in the smart contract so far (a mint or a transfer from/to that address, the address is set as an operator, the address becomes (un)managed, etc.). This table associates to these addresses the list of tokens they own and a flag to tell if they are managed wallets.
  - the `operators` associates to the pairs `(token_id, owner)` the list of addresses that are registered as operators.
  - the `state` table contains one raw. If provides some global information about the contract, like the `company_wallet`'s address, the `admin_wallet`, the number of existing (minted but burnt) tokens, the contract's address, the `last_included_token_id` and `last_included_meta_token_id` in a block (not necessarily confirmed).

To get more details about these tables's fields, you may want to use `\d <table_name>` in `psql` session, have a look at updates.ml file, or to the working/final document.


## 8. LICENSE

The files in this repository, except src/contract/, are

```
    Copyright (c) 2021-2022, Functori <contact@functori.com>
```

and are released under the MIT License (See LICENSE file at the root of this repository).

The ML smart contracts in ./src/contract/ have been translated from the Ligo version available at https://github.com/oxheadalpha/smart-contracts. They are then
adapted/modified/extended for the needs of this project. These files are

```
    Copyright (c) 2020 TQ Tezos
    Copyright (c) 2021-2022, Functori <contact@functori.com>
```

and are released under the MIT License (See ./src/contract/LICENSE file).
