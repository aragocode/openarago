open Mligo

type token_id = nat
type meta_token_id = int (* to enforce typing discipline *)
type token_amount = nat

type transfer_destination = {
  to_ : address;
  token_id : token_id;
  amount : token_amount;
} [@@comb]

type transfer = {
  from_ : address;
  txs : transfer_destination list;
} [@@comb]

type balance_of_request = {
  owner : address;
  token_id : token_id;
} [@@comb]

type balance_of_response = {
  request : balance_of_request;
  balance : nat;
} [@@comb]

type balance_of_param = {
  requests : balance_of_request list;
  callback : (balance_of_response list) contract;
} [@@comb]

type operator_param = {
  owner : address;
  operator : address;
  token_id: token_id;
} [@@comb]

type operator_update =
  | Add_operator of operator_param
  | Remove_operator of operator_param
[@@comb]

type meta_token_info = (string, bytes) map

type token_metadata = {
  token_id : token_id; [@key "token_id"]
  token_info : meta_token_info; [@key "token_info"]
} [@@comb]

(*
One of the options to make token metadata discoverable is to declare
`token_metadata : token_metadata_storage` field inside the FA2 contract storage
*)

(**
Optional type to define view entry point to expose token_metadata on chain or
as an external view
 *)
type token_metadata_param = {
  token_id : token_id;
  handler : token_metadata -> unit;
} [@@comb]

type managed_param = {
  pk : key;
  signed_msg: signature
} [@@comb]

type fa2_entry_points =
  | Transfer of transfer list
  | Balance_of of balance_of_param
  | Update_operators of operator_update list
  | Managed of managed_param list

(*
 TZIP-16 contract metadata storage field type.
 The contract storage MUST have a field
 `metadata : contract_metadata`
*)
type contract_metadata = (string, bytes) big_map

(* FA2 hooks interface *)

type transfer_destination_descriptor = {
  to_ : address option;
  token_id : token_id;
  amount : token_amount;
} [@@comb]

type transfer_descriptor = {
  from_ : address option;
  txs : transfer_destination_descriptor list
}

type transfer_descriptor_param = {
  batch : transfer_descriptor list;
  operator : address;
} [@@comb]



(* customized storage *)

type ledger = (token_id, address) big_map

type operator_storage = ((address * (address * token_id)), unit) big_map
type managed_storage = (address, unit) big_map

type hashed_secret = bytes

type storage = {
  admin : address;
  company_wallet: address;
  pending_admin : address option;
  pending_company_wallet : address option;
  paused : bool;
  ledger : ledger;
  operators : operator_storage;
  managed : managed_storage;
  next_meta_token_id : meta_token_id;
  _NB_TOKENS_PER_META : nat;
  _META_TOKEN_SHIFT : int; (* Should be greater than _NB_TOKENS_PER_META *)

  meta_token_info : (meta_token_id, meta_token_info) big_map;
  meta_token_supply : (meta_token_id, token_amount) big_map;
  untransferable : (token_id * address, hashed_secret) big_map;
  metadata : (string, bytes) big_map;
}

let mk_token_id_base (s : storage) (mt_id: meta_token_id) : token_id =
  match is_nat (mt_id * s._META_TOKEN_SHIFT) with
  | Some id -> id
  | None -> (failwith "Unreachable" : token_amount)

let get_meta_token_id (s : storage) (tid: token_id) : meta_token_id =
  tid / s._META_TOKEN_SHIFT
