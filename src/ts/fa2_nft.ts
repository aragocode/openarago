import {
  TezosToolkit,
  BigMapAbstraction,
  OpKind,
  createTransferOperation,
  TransferParams,
  RPCOperation,
  createRevealOperation
} from "@taquito/taquito"
import { MichelsonV1Expression } from "@taquito/rpc"
import { encodeOpHash } from '@taquito/utils';
import { assert } from 'console';

/** Interfaces/types for smart contract's entrypoints parameters and storage */
export interface TransferDestination {
  to_: string;
  token_id: bigint;
  amount: bigint;
}

export interface Transfer {
  from_: string;
  txs: TransferDestination[];
}

export interface MintParam {
  owner: string;
  token_id: bigint;
}

export interface MetaMintParam {
  meta_token_id: bigint;
  token_info: { [key: string]: string };
  tokens: MintParam[]
}

export interface SetUntransferableParam {
  set_token_id: bigint;
  set_owner: string;
  set_hash: string
}

export interface UnsetUntransferableParam {
  unset_token_id: bigint;
  unset_owner: string;
  unset_secret: string;
  unset_nonce: string;
}

export interface OperatorParam {
  owner: string;
  operator: string;
  token_id: bigint;
}

export interface BalanceOfRequest {
  owner: string;
  token_id: bigint;
}

export interface BalanceOfParam {
  requests: BalanceOfRequest[];
  callback: string;
}

export interface TokenMetadataParam {
  token_id: bigint;
  handler: MichelsonV1Expression[];
}

export interface ManagedParam {
  pk: string,
  signed_msg: string
}

export interface BatchParam {
  kind: 'set_admin' | 'set_company_wallet' | 'confirm_admin' | 'confirm_company_wallet' | 'pause' | 'managed' |
  'transfer' | 'update_operators' | 'mint_tokens' | 'burn_tokens' | 'balance_of' | 'token_metadata' |
  'set_untransferable' | 'unset_untransferable';
  param: string | undefined | boolean | Transfer[] | [OperatorParam, boolean][] | MetaMintParam[] | bigint[] |
  BalanceOfParam | TokenMetadataParam | SetUntransferableParam[] | UnsetUntransferableParam[] | ManagedParam[]
}

export interface Storage {
  admin: string;
  company_wallet: string;
  pending_admin: string | undefined | null;
  pending_company_wallet: string | undefined | null;
  paused: boolean;
  ledger: BigMapAbstraction;
  operators: BigMapAbstraction;
  managed: BigMapAbstraction;
  next_meta_token_id: number;
  _NB_TOKENS_PER_META : number;
  _META_TOKEN_SHIFT : number;

  meta_token_info : BigMapAbstraction;
  meta_token_supply : BigMapAbstraction;
  untransferable : BigMapAbstraction;
  metadata : BigMapAbstraction;
}

/** Auxiliary functions to encode entrypoints' parameters into Michelson */
export function set_admin_param(admin: string): MichelsonV1Expression {
  return { string: admin }
}

export function confirm_admin_param(): MichelsonV1Expression {
  return { prim: 'Unit' }
}

export function set_company_wallet_param(wallet: string): MichelsonV1Expression {
  return { string: wallet }
}

export function confirm_company_wallet_param(): MichelsonV1Expression {
  return { prim: 'Unit' }
}

export function pause_param(b: boolean): MichelsonV1Expression {
  if (b) { return { prim: 'True' } } else { return { prim: 'False' } }
}

export function managed_param(ml: ManagedParam[]): MichelsonV1Expression {
  return (
    ml.map(function (m) {
      return {
        prim: 'Pair',
        args: [{ string: m.pk }, { string: m.signed_msg }]
      };
    })
  );
}

export function set_untransferable_param(pl: SetUntransferableParam[]): MichelsonV1Expression {
  return pl.map(function (p) {
    return {
      prim: 'Pair',
      args: [
        { int: p.set_token_id.toString() },
        { string: p.set_owner },
        { bytes: p.set_hash }
      ]
    }
  })
}

export function unset_untransferable_param(pl: UnsetUntransferableParam[]): MichelsonV1Expression {
  return pl.map(function (p) {
    return {
      prim: 'Pair',
      args: [
        { int: p.unset_token_id.toString() },
        { string: p.unset_owner },
        { bytes: p.unset_secret },
        { bytes: p.unset_nonce },
      ]
    }
  })
}

export function transfer_param(trs: Transfer[]): MichelsonV1Expression {
  return trs.map(function (t) {
    return {
      prim: 'Pair', args: [
        { string: t.from_ },
        t.txs.map(function (tx) {
          return {
            prim: 'Pair', args: [
              { string: tx.to_ },
              { int: tx.token_id.toString() },
              { int: tx.amount.toString() }
            ]
          }
        })
      ]
    }
  })
}

export function update_operators_param(ops: [OperatorParam, boolean][]): MichelsonV1Expression {
  return ops.map(function ([p, add]) {
    const m = {
      prim: 'Pair', args: [
        { string: p.owner },
        { string: p.operator },
        { int: p.token_id.toString() }
      ]
    }
    if (add) { return { prim: 'Left', args: [m] } }
    else { return { prim: 'Right', args: [m] } }
  })
}

export function burn_param(p: bigint[]): MichelsonV1Expression {
  return p.map(function (token_id) { return { int: token_id.toString() } })
}

export function meta_mint_param(p: MetaMintParam[]): MichelsonV1Expression {
  return p.map(function (mp) {
    return {
      prim: 'Pair',
      args: [
        { int: mp.meta_token_id.toString() },
        Object.keys(mp.token_info).sort().map(function (k) {
          return {
            prim: 'Elt', args: [
              { string: k },
              { bytes: mp.token_info[k] }]
          }
        }),
        mp.tokens.map(function (mp) {
          return {
            prim: 'Pair',
            args: [
              { string: mp.owner },
              { int: mp.token_id.toString() }
            ]
          }
        })
      ]
    }
  });
}

export function balance_of_param(req: BalanceOfParam): MichelsonV1Expression {
  return {
    prim: 'Pair',
    args: [
      req.requests.map(function (e) {
        return {
          prim: 'Pair',
          args: [
            { string: e.owner },
            { int: e.token_id.toString() }
          ]
        }
      }),
      { string: req.callback }
    ]
  }
}

export function token_metadata_param(p: TokenMetadataParam): MichelsonV1Expression {
  return {
    prim: 'Pair',
    args: [
      { int: p.token_id.toString() },
      p.handler
    ]
  }
}

/** Some wrapper functions on top of Taquito to make calls, transfer xtz and reveal public keys.
 * With these functions, we have a better control on the operations hashs being injected.
*/

export interface operation_result {
  hash: string | null;
  error: any;
}

function toStrRec(input: any): any {
  Object.keys(input).forEach(k => {
    let elt = input[k]
    if (elt === undefined || elt === null) {
      input[k] = undefined
    } else if (typeof elt === 'object') {
      input[k] = toStrRec(elt);
    } else {
      input[k] = elt.toString();
    }
  });
  return input;
}

/** This function allows to reveal an address's public key on the blockchain.
 * The address is supposed to have some XTZ on it for the revelation to work.
 * The functions throws "WalletAlreadyRevealed" if the public key is already
 * revealed.
 * @param tk : Tezos toolkit
 * @returns injection result
 */
export async function revealWallet(tk: TezosToolkit): Promise<operation_result> {
  var opHash = null;
  try {
    let estimate = await tk.estimate.reveal();
    if (estimate == undefined) {
      throw "WalletAlreadyRevealed";
    }
    let publicKey = await tk.signer.publicKey();
    let source = await tk.signer.publicKeyHash();
    let revealParams = {
      fee: estimate.suggestedFeeMutez,
      gasLimit: estimate.gasLimit,
      storageLimit: estimate.storageLimit
    };
    let rpcRevealOperation = await createRevealOperation(revealParams, source, publicKey);
    let header = await tk.rpc.getBlockHeader();
    let contract = await tk.rpc.getContract(source);
    let counter = parseInt(contract.counter || '0', 10)
    let op = toStrRec({
      branch: header.hash,
      contents: [{
        ...rpcRevealOperation,
        source,
        counter: counter + 1
      }]
    });
    let forgedOp = await tk.rpc.forgeOperations(op)
    let signOp = await tk.signer.sign(forgedOp, new Uint8Array([3]));
    opHash = encodeOpHash(signOp.sbytes);
    const injectedOpHash = await tk.rpc.injectOperation(signOp.sbytes)
    assert(injectedOpHash == opHash);
    return { hash: opHash, error: null }
  } catch (error) {
    return { hash: opHash, error: error }
  }
}

/**
 * Generic auxiliary function for transfers and contracts calls
 * @param tk : Tezos toolkit
 * @param transferParams : the transfer's parameters
 * @returns injection result
 */
async function make_transactions(tk: TezosToolkit, transfersParams: Array<TransferParams>): Promise<operation_result> {
  var opHash = null;
  try {
    let source = await tk.signer.publicKeyHash();
    let contract = await tk.rpc.getContract(source);
    let counter = parseInt(contract.counter || '0', 10)
    let contents: Array<RPCOperation> = []
    await Promise.all(
      transfersParams.map(async function (transferParams) {
        let estimate = await tk.estimate.transfer(transferParams);
        const rpcTransferOperation = await createTransferOperation({
          ...transferParams,
          fee: estimate.suggestedFeeMutez,
          gasLimit: estimate.gasLimit,
          storageLimit: estimate.storageLimit
        });
        counter++;
        let v = {
          ...rpcTransferOperation,
          source,
          counter: counter,
        };
        contents.push(v)
      }));
    let header = await tk.rpc.getBlockHeader();
    let op = toStrRec({
      branch: header.hash,
      contents: contents
    })
    let forgedOp = await tk.rpc.forgeOperations(op)
    let signOp = await tk.signer.sign(forgedOp, new Uint8Array([3]));
    opHash = encodeOpHash(signOp.sbytes);
    let injectedoOpHash = await tk.rpc.injectOperation(signOp.sbytes)
    assert(injectedoOpHash == opHash);
    return { hash: opHash, error: null }
  } catch (error) {
    return { hash: opHash, error: error }
  }
}

/**
 * Instantiation of function `make_call` to transfer the given amount of
 * xtz from an account to the others.
 * @param tk : Tezos toolkit
 * @param destinations : the transfers' destinations
 * @param amount: the amount to transfer in Tez (will be converted internally to muTez)
 * @returns operation injection result
 */
export async function transfer_xtz(tk: TezosToolkit, destinations: string[], amount: number): Promise<operation_result> {
  try {
    var dests: TransferParams[] = [];
    destinations.forEach(function (dest) {
      let e = { amount: amount, to: dest };
      dests.push(e)
    });
    return await make_transactions(tk, dests);
  } catch (error) {
    return { hash: null, error: error };
  }
}

export async function send(
  tk: TezosToolkit,
  kt1: string,
  entrypoint: string,
  value: MichelsonV1Expression): Promise<operation_result> {
  try {
    return await make_transactions(tk, [{
      amount: 0,
      to: kt1,
      parameter: { entrypoint, value }
    }]);
  } catch (error) {
    return { hash: null, error: error };
  }
}
// await op.confirmation(confirmations);

export async function set_admin(
  tk: TezosToolkit,
  kt1: string,
  admin: string)
  : Promise<operation_result> {
  return send(tk, kt1, 'set_admin', set_admin_param(admin))
}

export async function confirm_admin(
  tk: TezosToolkit,
  kt1: string)
  : Promise<operation_result> {
  return send(tk, kt1, 'confirm_admin', confirm_admin_param())
}

export async function set_company_wallet(
  tk: TezosToolkit,
  kt1: string,
  wallet: string)
  : Promise<operation_result> {
  return send(tk, kt1, 'set_company_wallet', set_company_wallet_param(wallet))
}

export async function confirm_company_wallet(
  tk: TezosToolkit,
  kt1: string)
  : Promise<operation_result> {
  return send(tk, kt1, 'confirm_company_wallet', confirm_company_wallet_param())
}

export async function pause(
  tk: TezosToolkit,
  kt1: string)
  : Promise<operation_result> {
  return send(tk, kt1, 'pause', pause_param(true))
}

export async function unpause(
  tk: TezosToolkit,
  kt1: string)
  : Promise<operation_result> {
  return send(tk, kt1, 'pause', pause_param(false))
}

export async function managed(
  tk: TezosToolkit,
  kt1: string,
  p: ManagedParam[])
  : Promise<operation_result> {
  return send(tk, kt1, 'managed', managed_param(p));
}

export async function set_untransferable(
  tk: TezosToolkit,
  kt1: string,
  p: SetUntransferableParam[])
  : Promise<operation_result> {
  return send(tk, kt1, 'set_untransferable', set_untransferable_param(p))
}

export async function unset_untransferable(
  tk: TezosToolkit,
  kt1: string,
  p: UnsetUntransferableParam[])
  : Promise<operation_result> {
  return send(tk, kt1, 'unset_untransferable', unset_untransferable_param(p))
}

export async function transfer(
  tk: TezosToolkit,
  kt1: string,
  txs: Transfer[])
  : Promise<operation_result> {
  return send(tk, kt1, 'transfer', transfer_param(txs))
}

export async function update_operators(
  tk: TezosToolkit,
  kt1: string,
  ops: [OperatorParam, boolean][])
  : Promise<operation_result> {
  return send(tk, kt1, 'update_operators', update_operators_param(ops))
}

export async function mint(
  tk: TezosToolkit,
  kt1: string,
  p: MetaMintParam[])
  : Promise<operation_result> {
  return send(tk, kt1, 'mint_tokens', meta_mint_param(p))
}

export async function burn(
  tk: TezosToolkit,
  kt1: string,
  p: bigint[])
  : Promise<operation_result> {
  return send(tk, kt1, 'burn_tokens', burn_param(p))
}

export async function balance_of(
  tk: TezosToolkit,
  kt1: string,
  txs: BalanceOfParam)
  : Promise<operation_result> {
  return send(tk, kt1, 'balance_of', balance_of_param(txs))
}

export async function token_metadata(
  tk: TezosToolkit,
  kt1: string,
  p: TokenMetadataParam)
  : Promise<operation_result> {
  return send(tk, kt1, 'token_metadata', token_metadata_param(p))
}

export async function batch(
  tk: TezosToolkit,
  kt1: string,
  p: BatchParam[])
  : Promise<operation_result> {
  const l = p.map(function (bp) {
    switch (bp.kind) {
      case 'set_admin':
        let v_set_admin = <string>bp.param
        return { entrypoint: bp.kind, value: set_admin_param(v_set_admin) }
      case 'set_company_wallet':
        let v_set_company_wallet = <string>bp.param
        return { entrypoint: bp.kind, value: set_admin_param(v_set_company_wallet) }
      case 'confirm_admin':
        return { entrypoint: bp.kind, value: confirm_admin_param() }
      case 'confirm_company_wallet':
        return { entrypoint: bp.kind, value: confirm_company_wallet_param() }
      case 'pause':
        let v_pause = <boolean>bp.param
        return { entrypoint: bp.kind, value: pause_param(v_pause) }
      case 'managed':
        let v_managed = <ManagedParam[]>bp.param
        return { entrypoint: bp.kind, value: managed_param(v_managed) }
      case 'transfer':
        let v_transfer = <Transfer[]>bp.param
        return { entrypoint: bp.kind, value: transfer_param(v_transfer) }
      case 'update_operators':
        let v_update_operators = <[OperatorParam, boolean][]>bp.param
        return { entrypoint: bp.kind, value: update_operators_param(v_update_operators) }
      case 'mint_tokens':
        let v_mint = <MetaMintParam[]>bp.param
        return { entrypoint: bp.kind, value: meta_mint_param(v_mint) }
      case 'burn_tokens':
        let v_burn = <bigint[]>bp.param
        return { entrypoint: bp.kind, value: burn_param(v_burn) }
      case 'balance_of':
        let v_balOf = <BalanceOfParam>bp.param
        return { entrypoint: bp.kind, value: balance_of_param(v_balOf) }
      case 'token_metadata':
        let vTokenMeta = <TokenMetadataParam>bp.param
        return { entrypoint: bp.kind, value: token_metadata_param(vTokenMeta) }
      case 'set_untransferable':
        let puntr = <SetUntransferableParam[]>bp.param
        return { entrypoint: bp.kind, value: set_untransferable_param(puntr) }
      case 'unset_untransferable':
        let ptr = <UnsetUntransferableParam[]>bp.param
        return { entrypoint: bp.kind, value: unset_untransferable_param(ptr) }
      default:
        throw `batch: switch not exhaustive. Case ${bp.kind} not covered`
    }
  })
  const params = l.map(function (parameter) {
    return {
      //kind: <OpKind.TRANSACTION>OpKind.TRANSACTION,
      amount: 0,
      to: kt1,
      parameter
    }
  })
  try {
    return await make_transactions(tk, params);
  } catch (error) {
    return { hash: null, error: error };
  }
}

export async function storage(
  tk: TezosToolkit,
  kt1: string)
  : Promise<Storage> {
  const contract = await tk.contract.at(kt1)
  let storage = await contract.storage() as Storage
  return storage
}

export async function owner(
  tk: TezosToolkit,
  kt1: string,
  token_id: number,
  storage?: Storage
)
  : Promise<string> {
  const contract = await tk.contract.at(kt1)
  let st = (storage == undefined) ? await contract.storage() as Storage : storage!
  let v = await st.ledger.get(token_id) as string
  return v
}

export async function owners(
  tk: TezosToolkit,
  kt1: string,
  token_ids: number[],
  storage?: Storage
)
  : Promise<any> {
  const contract = await tk.contract.at(kt1)
  let st = (storage == undefined) ? await contract.storage() as Storage : storage!
  let v = await st.ledger.getMultipleValues(token_ids)
  return v
}
